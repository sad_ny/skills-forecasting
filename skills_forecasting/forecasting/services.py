from datetime import datetime

from forecasting.config import AVG_GRADE_DELTA, MAX_GRADES
from skills.dao.skill_dao import get_employee_skills, get_employee_subskills, count_skills, count_sub_skills


# TODO: Смапить числа и названия грейдов для каждой должности и специализации
# TODO: Возвращать данные в виде названий гредов

# Посчитать минимальный уровень и количество (саб)скиллов
def calculate_level_count(employee):
    sl = get_employee_skills(employee)
    ssl = get_employee_subskills(employee)

    # Минимальный уровень скиллов
    min_lv = MAX_GRADES

    # Если нет записей о скиллах сотрудника, принимаем уровень за 0
    if len(sl) + len(ssl) == 0:
        min_lv = 0

    for l in sl:
        if l[2] < min_lv:
            min_lv = l[2]
    for l in ssl:
        if l[2] < min_lv:
            min_lv = l[2]

    # Количество скиллов на уровень выше, которые есть у сотрудника
    count = 0
    for l in sl:
        if l[2] == min_lv + 1:
            count += 1
    for l in ssl:
        if l[2] == min_lv + 1:
            count += 1

    return min_lv, count


# Посчитать отношение всех скиллов сотрудника к требуемым
def calculate_alpha(employee):
    min_lv, emp_count = calculate_level_count(employee)
    count = count_skills(employee.position, min_lv + 1) + count_sub_skills(employee.sub_position, min_lv + 1)
    if count > 0:
        alpha = 1 - emp_count / count
    else:
        alpha = 0
    return alpha, min_lv


# Предсказать куда вырастет наш сотрудник к определённому моменту
def forecast_date(employee, date):
    # Вычислить alpha коэфф для сотрудника
    alpha, min_lv = calculate_alpha(employee)

    # Сколько дней понадобится сотруднику для следующего грейда
    fc_timedelta = alpha * AVG_GRADE_DELTA

    # Посчитать сколько грейдов умещается в указанную дату
    grade = min_lv
    cur_dt = datetime.now()
    if cur_dt + fc_timedelta < date:
        cur_dt += fc_timedelta
        grade += 1

    # TODO: Остановиться на последнем для должности сотрудника грейде, иначе прогноз идёт до бесконечности
    while cur_dt + AVG_GRADE_DELTA < date:
        cur_dt += AVG_GRADE_DELTA
        grade += 1

    return grade


# Предсказать через какой период времени вырастет наш сотрудник
def forecast_period(employee):
    # Вычислить alpha коэфф для сотрудника
    alpha, min_lv = calculate_alpha(employee)

    # Сколько дней понадобится сотруднику для следующего грейда
    fc_timedelta = alpha * AVG_GRADE_DELTA

    return datetime.now() + fc_timedelta


# Предсказать количество человек для каждого грейда к определённому моменту
def forecast_group(employees, date):
    lv_map = {}
    for employee in employees:
        lv = str(forecast_date(employee, date))
        if lv_map.get(lv) is None:
            lv_map[lv] = 1
        else:
            lv_map[lv] += 1

    return lv_map
