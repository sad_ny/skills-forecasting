# Минимальное значение alpha если вычисленное для сотрудника alpha = 0
from datetime import timedelta

ALPHA_MIN = 0.01

# Среднее количество лет для получения нового грейда
AVG_GRADE_DELTA = timedelta(days=730)

# Максимальное количество уровней грейдов
MAX_GRADES = 5
