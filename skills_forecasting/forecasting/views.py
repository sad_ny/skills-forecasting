from datetime import datetime

from django.http import JsonResponse

from employee.models import Employee
from forecasting.services import forecast_period, forecast_date, forecast_group


def forecast_employee_view(request, id):
    employee = Employee.objects.get(pk=id)
    raw_date = request.GET.get('date')
    if raw_date is not None:
        date = datetime.strptime(raw_date, '%Y-%m-%d')
        return JsonResponse({"next_grade": forecast_date(employee, date)})
    return JsonResponse({"next_grade_in": forecast_period(employee)})


def forecast_employees_view(request):
    employees = Employee.objects.all()
    date = datetime.strptime(request.GET.get('date'), '%Y-%m-%d')
    print(forecast_group(employees, date))
    return JsonResponse(forecast_group(employees, date))
