from django.urls import path

from forecasting import views

urlpatterns = [
    path('<int:id>', views.forecast_employee_view, name='forecast_employee'),
    path('', views.forecast_employees_view, name='forecast_employees'),
]
