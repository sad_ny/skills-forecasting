from django.urls import path

from . import views

urlpatterns = [
    path('<int:id>/statistics', views.department_statistics_view, name='department'),
    path('<int:department_id>/position/<int:position_id>/statistics', views.position_statistics_view, name='position')
]
