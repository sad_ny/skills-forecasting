from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from employee.models import Department
from skills.models import Position, SubPosition
from department import template_helpers


def department_statistics_view(request, id):
    # Список статистик по специальности
    positions = []
    for position in Position.objects.all():
        positions.append(template_helpers.PositionStatistics(position, id))

    return render(request, "department/statistics.html",
                  {"objects": template_helpers.convert_to_rows(positions),
                   "header": get_object_or_404(Department, id=id),
                   "type": "department"})


def position_statistics_view(request, department_id, position_id):
    # Список статистик по должности
    sub_positions = []
    for sub_position in SubPosition.objects.filter(position_id=position_id):
        sub_positions.append(template_helpers.SubPositionStatistics(sub_position, department_id, position_id))

    return render(request, "department/statistics.html",
                  {"objects": template_helpers.convert_to_rows(sub_positions),
                   "header": get_object_or_404(Position, id=position_id),
                   "type": "position"})
