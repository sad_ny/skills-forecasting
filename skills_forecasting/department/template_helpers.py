from employee.models import Employee
from skills.models import SubPosition


"""Вывод таблицы в шаблоне"""


# Функция для удобного рендеринга таблицы в templates
def convert_to_rows(object_list):
    rows = []
    first = True
    for object in object_list:
        for sub_object in object.items_list:
            rows.append(ObjectRow(
                object.object,
                sub_object,
                object.items_list[sub_object],
                len(object.items_list) if first else None))
            first = False
        first = True
    return rows


# Строка таблицы
class ObjectRow:

    def __init__(self, object, sub_object, count, rowspan_value_if_first=None):
        self.object = object
        self.sub_object = sub_object
        self.count = count
        self.first = rowspan_value_if_first


# Родительский класс статистики
class Statistics:

    def __init__(self, object, getter=None):
        self.object = object
        self.items_list = getter

    def __str__(self):
        return "(%s, %s)" % (self.object, self.items_list)


# Объект этого класса - специальность со всеми должностями и их статистикой
class PositionStatistics(Statistics):

    def __init__(self, position, department_id):
        super().__init__(position)
        self.items_list = self.get_sub_positions(department_id)

    def get_employees(self, department_id):
        return Employee.objects.filter(department_id=department_id, position=self.object)

    def get_all_sub_positions(self):
        return SubPosition.objects.filter(position=self.object)

    # Подсчёт статистики для специальности
    def get_sub_positions(self, department_id):
        sub_positions = {}
        for sub_position in self.get_all_sub_positions():
            sub_positions[sub_position] = 0

        for employee in self.get_employees(department_id):
            sub_positions[employee.sub_position] += 1

        return sub_positions


# Объект этого класса - должность со всеми уровнями и их статистикой
class SubPositionStatistics(Statistics):

    def __init__(self, sub_position, department_id, position_id):
        super().__init__(sub_position)
        self.items_list = self.get_levels(department_id, position_id)  # list of levels

    def get_employees(self, department_id, position_id):
        return Employee.objects.filter(department_id=department_id, position_id=position_id, sub_position=self.object)

    # Подсчёт статистики для должности
    def get_levels(self, department_id, position_id):
        levels = {}
        min_level, max_level = 1, 4
        for i in range(min_level, max_level):
            levels[i] = 0
        for employee in self.get_employees(department_id, position_id):
            employee_sub_position_level = int(employee.level)
            levels[employee_sub_position_level] += 1
        return levels
