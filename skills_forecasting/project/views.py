from django.shortcuts import render
from project.dao import project_dao
from employee import helpers as employee_helpers


def project_view(request, id):
    auth_user = request.user
    project = project_dao.get_project_by_id(id)
    members = project_dao.get_project_members(project)
    sorted_members = project_dao.sort_members_by_role_in_team(members)
    technologies = project_dao.get_all_project_technologies(project)
    is_lead = employee_helpers.is_lead(request, id)
    rare_skills = project_dao.get_rare_skills(members)
    technology_statistics = project_dao.get_technology_statistics(members, technologies)
    team_skills_statistics = project_dao.get_team_skills_statistics(members)
    return render(request, "project/project.html", locals())


