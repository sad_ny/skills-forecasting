from employee.models import *
from skills.dao import skill_dao
from department.template_helpers import convert_to_rows, Statistics


def get_project_by_id(id):
    return Project.objects.get(pk=id)


def get_all_project_technologies(project):
    return project.technologies.all()


def get_project_members(project):
    hired_staff = list(map(lambda staff: staff.employee, list(project.projects_staff.all())))
    project_team = list(Employee.objects.filter(team=project.team))
    return project_team + hired_staff


def sort_members_by_role_in_team(members):
    sorted_members = {}
    for member in members:
        if member.role == "emp" or member.role == "lol" or member.role == "com":
            sorted_members[member] = 0
        elif member.role == "lid":
            sorted_members[member] = 1
    sorted_members_list = list(sorted_members.items())
    sorted_members_list.sort(key=lambda i: -i[1])
    result = []
    for member in sorted_members_list:
        result.append(member[0])
    return result


def get_rare_skills(members):
    all_skills = {}
    for member in members:
        member_skills = skill_dao.get_employee_skills(member)
        for skill in member_skills:
            if (all_skills.get(skill[1]) == None):
                all_skills[skill[1]] = [1, [member]]
            else:
                all_skills[skill[1]][0] += 1
                all_skills[skill[1]][1].append(member)
    skills_list = list(all_skills.items())
    skills_list.sort(key=lambda i: i[1][0])
    rare_list = skills_list[:3]
    return rare_list


def get_technology_statistics(members, technologies):
    technologies_ = []

    for technology in technologies:
        employees_ssl = EmployeeSubSkillLevel.objects.filter(sub_skill_level__sub_skill=technology.sub_skill)
        members_with_technology = [employee_ssl for employee_ssl in employees_ssl if employee_ssl.employee in members]
        technologies_.append(TechnologyStatistics(technology.sub_skill, members_with_technology))
    return convert_to_rows(technologies_)


def get_team_skills_statistics(members):
    skills_set = set()
    for member in members:
        skills_set.update([employee_skill_level.sub_skill_level.sub_skill
                            for employee_skill_level in EmployeeSubSkillLevel.objects.filter(employee=member)])
    skills = []
    for skill in skills_set:
        employees_ssl = EmployeeSubSkillLevel.objects.filter(sub_skill_level__sub_skill=skill)
        members_with_skill = [employee_ssl for employee_ssl in employees_ssl if employee_ssl.employee in members]
        skills.append(TeamSkillsStatistics(skill, members_with_skill))
    return convert_to_rows(skills)


class TechnologyStatistics(Statistics):

    def __init__(self, technology, employees_sub_skill_levels):
        super().__init__(technology, get_employees_with_skill_levels(employees_sub_skill_levels))


class TeamSkillsStatistics(Statistics):

    def __init__(self, skill, employees_with_skill):
        super().__init__(skill, get_employees_with_skill_levels(employees_with_skill))


def get_employees_with_skill_levels(employees_with_skill):
    employees_sub_skill_levels_ = {}
    for employee in employees_with_skill:
        employees_sub_skill_levels_[str(employee.employee.user.last_name)
                                    + " " + str(employee.employee.user.first_name)] = employee.sub_skill_level.name
    return employees_sub_skill_levels_
