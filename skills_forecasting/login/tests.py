import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

class LoginTest(TestCase):
    def setUp(self):
        self.client = Client()
        User = get_user_model()
        User.objects.create_user(
            username='admin', password='admin', first_name='user', last_name='user', birth_date=datetime.date.today())

    def test_login(self):
        login_stat = self.client.login(username="admin", password="admin")
        self.assertEqual(login_stat, True)

    def test_redirect(self):
        login_stat = self.client.login(username="admin", password="admin")
        response = self.client.get("/login")
        self.assertRedirects(response, "employee/1")
