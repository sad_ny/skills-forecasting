from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login


def login(request):
    args = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect('/employee/' + str(user.id))
        else:
            args['login_error'] = "Пользователь не найден"
            return render(request, 'login/login.html', args)
    elif request.method == 'GET':
        user = auth.get_user(request)
        if user.is_authenticated:
            return redirect('/employee/' + str(user.id))
        return render(request, 'login/login.html', args)


def logout(request):
    if auth.get_user(request).is_authenticated:
        auth_logout(request)
    return redirect('login')

