from django.db.models import Q

from login.models import *


def get_user_by_id(id):
    return User.objects.get(pk=id)


def get_users_by_fio_substring(string):
    fio = string.split(" ")
    q = None
    if len(fio) <= 3:
        for word in fio:
            q1 = Q(first_name__contains=word)
            q2 = Q(last_name__contains=word)
            q3 = Q(middle_name__contains=word)
            q = q & (q1 | q2 | q3) if q else q1 | q2 | q3
    return User.objects.filter(q)


def set_profile_access(user, closed):
    user.closed = True if closed else False
    user.save()
