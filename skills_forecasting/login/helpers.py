from _datetime import date


"""template view methods"""


def get_user_birthday(user):
    return convert_to_CIS_date_format(user.birth_date)


"""some functions"""


def convert_to_CIS_date_format(some_date):
    return date.strftime(some_date, "%d.%m.%Y")
