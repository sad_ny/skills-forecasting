from skills.models import *
from employee.models import *

"""dao methods"""


def get_all_employee_skills(employee):
    skills = []
    for esl in EmployeeSkillLevel.objects.filter(employee=employee):
        skill = [esl.datetime]
        sl = esl.skill_level
        skill.append(sl.name)
        skill.append(sl.weight)
        sk = sl.skill
        skill.append(sk.name)
        skill.append('skill')
        skill.reverse()
        skills.append(skill)
    for usl in UserSoftSkill.objects.filter(estimated=employee.user):
        skill = [usl.value, usl.estimator, usl.soft_skill.name, 'softskill']
        skill.reverse()
        skills.append(skill)
    for esubl in EmployeeSubSkillLevel.objects.filter(employee=employee):
        skill = [esubl.datetime]
        sl = esubl.sub_skill_level
        skill.append(sl.name)
        skill.append(sl.weight)
        sk = sl.sub_skill
        skill.append(sk.name)
        skill.append('subskill')
        skill.reverse()
        skills.append(skill)
    return skills


# Метод для получения SKILL'ов
def get_employee_skills(employee):
    # Best sl
    sl = [[l.datetime, l.skill_level] for l in employee.skill_levels.all()]
    sl_map = {}
    for l in sl:
        if sl_map.get(l[1].skill) is not None:
            if l[1].weight > sl_map.get(l[1].skill)[1].weight:
                sl_map[l[1].skill] = l
        else:
            sl_map[l[1].skill] = l

    sl = []

    for key in sl_map:
        skill = []
        skill.append(sl_map[key][0])
        skill.append(sl_map[key][1].name)
        skill.append(sl_map[key][1].weight)
        skill.append(sl_map[key][1].skill.name)
        skill.append('skill')
        skill.reverse()
        sl.append(skill)

    return sl


# Метод для получения SUBSKILL'ов
def get_employee_subskills(employee):
    # Best ssl
    ssl = [[l.datetime, l.sub_skill_level] for l in employee.sub_skill_levels.all()]
    ssl_map = {}
    for l in ssl:
        if ssl_map.get(l[1].sub_skill) is not None:
            if l[1].weight > ssl_map.get(l[1].sub_skill)[1].weight:
                ssl_map[l[1].sub_skill] = l
        else:
            ssl_map[l[1].sub_skill] = l

    ssl = []

    for key in ssl_map:
        skill = []
        skill.append(ssl_map[key][0])
        skill.append(ssl_map[key][1].name)
        skill.append(ssl_map[key][1].weight)
        skill.append(ssl_map[key][1].sub_skill.name)
        skill.append('subskill')
        skill.reverse()
        ssl.append(skill)

    return ssl


# Метод для получения SOFTSKILL'ов
def get_employee_softskills(employee):
    softskills = []
    all_skills = get_all_employee_skills(employee)
    for skill in all_skills:
        if skill[0] == 'softskill':
            softskills.append(skill)
    return softskills


def get_recommendations(employee):

    # Вытаскиваем (саб)скиллы из промежуточной таблицы
    ssl = [l.sub_skill_level for l in employee.sub_skill_levels.all().order_by('sub_skill_level__weight').order_by(
        'sub_skill_level__sub_skill_id')]
    sl = [l.skill_level for l in
          employee.skill_levels.all().order_by('skill_level__weight').order_by('skill_level__skill_id')]

    # Находим минимальный уровень скиллов
    min_lv = 10
    min_id = 0

    for l in ssl:
        if l.weight < min_lv:
            min_id = l.sub_skill_id
            min_lv = l.weight
        elif l.sub_skill_id == min_id:
            min_lv = l.weight

    for l in sl:
        if l.weight < min_lv:
            min_id = l.skill_id
            min_lv = l.weight
        elif l.skill_id == min_id:
            min_lv = l.weight

    # Вытаскиваем айдишники (саб)скиллов, где уровень на 1 больше минимального
    ssl_excluded_ids = [l.pk for l in filter(lambda x: x.weight == min_lv + 1, ssl)]
    sl_excluded_ids = [l.pk for l in filter(lambda x: x.weight == min_lv + 1, sl)]

    # Собираем (саб)скиллы с уровнем на 1 больше, исключаем уже имеющиеся у сотрудника по айдишнику
    rec_ssl = SubSkillLevel.objects.filter(weight=min_lv + 1).exclude(id__in=ssl_excluded_ids)
    rec_sl = SkillLevel.objects.filter(weight=min_lv + 1).exclude(id__in=sl_excluded_ids)

    rec_ssl = rec_ssl.filter(sub_skill__sub_position=employee.sub_position)
    rec_sl = rec_sl.filter(skill__position=employee.position)

    return rec_sl, rec_ssl


# Взять скиллы конкретного типа (базовые, гибкие, специализации)
def get_skills(tab):
    if tab == 1:
        return Skill.objects.all()
    elif tab == 2:
        return SubSkill.objects.all()
    elif tab == 3:
        return SoftSkill.objects.all()


# Взять скиллы конкретного типа (базовые, гибкие, специализации)
def get_skills_by_section(tab, section_id):
    if tab == 1:
        return Skill.objects.filter(position_id=section_id)
    elif tab == 2:
        return SubSkill.objects.filter(sub_position_id=section_id)
    elif tab == 3:
        return SoftSkill.objects.all()


# Взять скилл конкретного типа с известным именем и уровнем и его раздел
def get_skill_and_section(tab, name):
    if tab == 1:
        skill = Skill.objects.get(name=name)
        return skill, skill.position
    elif tab == 2:
        sub_skill = SubSkill.objects.get(name=name)
        return sub_skill, sub_skill.sub_position
    elif tab == 3:
        return SoftSkill.objects.get(name=name), None


def get_sections(tab):
    if tab == 1:
        return Position.objects.all()
    elif tab == 2:
        return SubPosition.objects.all()
    elif tab == 3:
        return None


# Взять последние скилы сотрудника
def latest_skills(employee):
    # Составляем словарь последних полученных скиллов
    sl = [[l.datetime, l.skill_level] for l in employee.skill_levels.all()]
    sl_map = {}
    for l in sl:
        if sl_map.get(l[1].skill) is not None:
            if l[0] > sl_map.get(l[1].skill)[0]:
                sl_map[l[1].skill] = l
        else:
            sl_map[l[1].skill] = l

    sl = []

    for key in sl_map:
        skill = [sl_map[key][0], sl_map[key][1].name, sl_map[key][1].weight, sl_map[key][1].sub_skill.name, 'skill']
        skill.reverse()
        sl.append(skill)

    return sl


# Взять последние сабскилы сотрудника
def latest_sub_skills(employee):
    # Составляем словарь последних полученных скиллов
    ssl = [[l.datetime, l.sub_skill_level] for l in employee.sub_skill_levels.all()]
    ssl_map = {}
    for l in ssl:
        if ssl_map.get(l[1].sub_skill) is not None:
            if l[0] > ssl_map.get(l[1].sub_skill)[0]:
                ssl_map[l[1].sub_skill] = l
        else:
            ssl_map[l[1].sub_skill] = l

    ssl = []

    for key in ssl_map:
        skill = [ssl_map[key][0], ssl_map[key][1].name, ssl_map[key][1].weight, ssl_map[key][1].sub_skill.name,
                 'sub_skill']
        skill.reverse()
        ssl.append(skill)

    return ssl


# Посчитать навыки определенного уровня для должности
def count_skills(position, weight):
    return SkillLevel.objects.filter(skill__position=position).filter(weight=weight).count()


# Посчитать навыки определенного уровня для специализации
def count_sub_skills(sub_position, weight):
    return SubSkillLevel.objects.filter(sub_skill__sub_position=sub_position).filter(weight=weight).count()
