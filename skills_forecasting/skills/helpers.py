from skills.dao import skill_dao

import json

from skills.models import SkillLevel, SubSkillLevel


def convert_skills_to_json(employee, type):
    if type == 'softskill':
        softskills = skill_dao.get_employee_softskills(employee)
        for softskill in softskills:
            del softskill[0]
        softskills_json = json.dumps(softskills)
        return softskills_json
    elif type == 'subskill':
        subskills = skill_dao.get_employee_subskills(employee)
        for subskill in subskills:
            del subskill[-1]
            del subskill[0]
        subskills_json = json.dumps(subskills)
        return subskills_json
    else:
        skills = skill_dao.get_employee_skills(employee)
        for skill in skills:
            del skill[-1]
            del skill[0]
        skills_json = json.dumps(skills)
        return skills_json


def convert_recommend_to_json(recommends_set):
    recommends = list(recommends_set)
    parse_recommends = list()
    for recommend in recommends:
        if isinstance(recommend, SkillLevel):
            parse_recommend = [recommend.skill.name, recommend.name, recommend.description]
            parse_recommends.append(parse_recommend)
        elif isinstance(recommend,SubSkillLevel):
            parse_recommend = [recommend.sub_skill.name, recommend.name, recommend.description]
            parse_recommends.append(parse_recommend)
    return json.dumps(parse_recommends)
