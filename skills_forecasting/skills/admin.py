from django.contrib import admin

from skills.models import Position, Skill, SubPosition, SubSkill, \
    UserSoftSkill, SubSkillLevel, SoftSkill, SkillLevel

admin.site.register(Position)
admin.site.register(SubPosition)
admin.site.register(Skill)
admin.site.register(SubSkill)
admin.site.register(SkillLevel)
admin.site.register(SubSkillLevel)
admin.site.register(SoftSkill)
admin.site.register(UserSoftSkill)
