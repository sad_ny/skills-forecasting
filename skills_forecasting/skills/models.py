from django.db import models
from django.db.models import CASCADE
from login.models import User


class Position(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        verbose_name = 'Специальность'
        verbose_name_plural = 'Специальности'

    def __str__(self):
        return self.name


class SubPosition(models.Model):
    position = models.ForeignKey(Position, on_delete=CASCADE, related_name='sub_positions')
    name = models.CharField(max_length=128)

    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'

    def __str__(self):
        return self.name


class Skill(models.Model):
    position = models.ForeignKey(Position, on_delete=CASCADE, related_name='skills')
    name = models.CharField(max_length=128)

    class Meta:
        verbose_name = 'Базовый навык'
        verbose_name_plural = 'Базовые навыки'

    def __str__(self):
        return self.name


class SubSkill(models.Model):
    sub_position = models.ForeignKey(SubPosition, on_delete=CASCADE, related_name='sub_skills')
    name = models.CharField(max_length=128)

    class Meta:
        verbose_name = 'Навык специализации'
        verbose_name_plural = 'Навыки специализации'

    def __str__(self):
        return self.name


class SkillLevel(models.Model):
    skill = models.ForeignKey(Skill, on_delete=CASCADE, related_name='skill_levels')
    name = models.CharField(max_length=128)
    description = models.TextField()
    weight = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = 'Уровень общего навыка'
        verbose_name_plural = 'Уровни общих навыков'

    def __str__(self):
        return f"{self.skill} - {self.name}"


class SubSkillLevel(models.Model):
    sub_skill = models.ForeignKey(SubSkill, on_delete=CASCADE, related_name='sub_skill_levels')
    name = models.CharField(max_length=128)
    description = models.TextField()
    weight = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = 'Уровень навыка специализации'
        verbose_name_plural = 'Уровни навыков специализации'

    def __str__(self):
        return f"{self.sub_skill} - {self.name}"


class SoftSkill(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        verbose_name = 'Гибкий навык'
        verbose_name_plural = 'Гибкие навыки'

    def __str__(self):
        return self.name


class UserSoftSkill(models.Model):
    soft_skill = models.ForeignKey(SoftSkill, on_delete=CASCADE, related_name='estimations')
    estimator = models.ForeignKey(User, on_delete=CASCADE, related_name='my_est')
    estimated = models.ForeignKey(User, on_delete=CASCADE, related_name='other_est')
    value = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = 'Уровень гибкого навыка'
        verbose_name_plural = 'Уровни гибких навыков'

    def __str__(self):
        return f"{self.soft_skill} - {self.value}"
