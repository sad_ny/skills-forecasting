from _datetime import date
from functools import wraps

from django.http import HttpResponse

from employee.dao import employee_dao
from .models import *

"""template view methods"""


def calculate_experience(start_date):
    years = calculate_age(start_date)
    months = calculate_months(start_date)
    return convert_age_to_string(years) + " " + convert_months_to_string(months)


def convert_age_to_string(age):
    many = 'лет'
    few = 'года'
    one = 'год'
    return str(age) + ' ' + convert_from_cardinal_number_to_string(age, many, few, one)


def convert_months_to_string(age):
    many = 'месяцев'
    few = 'месяца'
    one = 'месяц'
    return str(age) + ' ' + convert_from_cardinal_number_to_string(age, many, few, one)


"""some functions"""


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def calculate_months(start_date):
    today = date.today()
    return today.month - start_date.month if today.month > start_date.month else start_date.month - today.month


def convert_from_cardinal_number_to_string(number, many, few, one):
    return many if number % 10 == 0 or 4 < number % 10 < 10 or 10 < number % 100 < 15 else few if 1 < number % 10 < 5 else one


def is_lofl(user):
    employees = employee_dao.get_employees_by_user(user)

    for employee in employees:
        if employee.role == 'lol':
            return True
    return False


def is_lead(request, id):
    employees = employee_dao.get_employees_by_user(request.user)
    lead_project_ids = []

    for employee in employees:
        if employee.role == 'lid'or employee.role == 'lom':
            projects = employee_dao.get_projects_by_team(employee)
            for project in projects:
                lead_project_ids.append(project.id)

    if id in lead_project_ids:
        return True
    else:
        return False


def get_projects_of_lead(user):
    employees = employee_dao.get_employees_by_user(user)
    lead_projects = []

    for employee in employees:
        if employee.role == 'lid'or employee.role == 'lom':
            projects = employee_dao.get_projects_by_team(employee)
            for project in projects:
                lead_projects.append(project)

    return lead_projects


def is_in_comission(user):
    employees = employee_dao.get_employees_by_user(user)

    for employee in employees:
        if (employee.role == 'lol') or (employee.role == 'com') or (employee.role == 'lom'):
            return True
    return False


def allowed_to_add_skills(request):
    employees = employee_dao.get_employees_by_user(request.user)

    for employee in employees:
        if (employee.role == 'lol') or (employee.role == 'com') or (employee.role == 'lom'):
            return True
    return False


"""decorators"""


def editor(f):
    @wraps(f)
    def wrap(request, *args, **kwargs):
        employees = employee_dao.get_employees_by_user(request.user)

        for employee in employees:
            if employee.role == 'lol' or employee.role == 'com' or employee.role == 'lom':
                return f(request, *args, **kwargs)

        return HttpResponse("Вы не можете добавлять скиллы, увы")

    return wrap

