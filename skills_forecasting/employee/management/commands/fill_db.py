from django.core.management.base import BaseCommand
from skills_forecasting.fill_db import fill_db


class Command(BaseCommand):
    def handle(self, *args, **options):
        fill_db()