from django.urls import path

from . import views

urlpatterns = [
    path('<int:id>', views.profile_view, name='profile'),
    path('<int:id>/edit/<int:tab>', views.edit_view, name='edit'),
    path('<int:id>/edit/<int:tab>/data', views.edit_skill_data_view, name='edit_skill_data'),
    path('<int:id>/recommendations', views.personal_recommendation_view, name='recommendations'),

    # URL для post запроса на изменение приватности профиля
    path('<int:id>/changeaccess', views.change_profile_access, name='change_profile_access'),

    # URL для данных сотрудника
    path('<int:id>/data', views.employee_data_view, name='data'),

    # Поиск сотрудников по имени
    path('search', views.employee_search_by_name_view, name='employee_search'),

    # Добавление сотрудников в команду/проект
    path('<int:id>/add_employee', views.lead_team_management_view, name='add_employee_team'),

    # Поиск сотрудников по скиллам в рекомендациях
    path('skill_search', views.search_by_skills_view, name='skill_search'),

    # Назначение сотрудника работником комиссии
    path('<int:id>/comission', views.make_comission_worker_view, name='comission'),
    path('<int:id>/comission/remove', views.remove_from_comission_view, name='comission_remove'),

]
