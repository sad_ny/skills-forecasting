from django.contrib import admin
from .models import *

admin.site.register(Job)
admin.site.register(Department)
admin.site.register(Team)
admin.site.register(Project)
admin.site.register(ProjectSubSkill)
admin.site.register(Division)
admin.site.register(Employee)
admin.site.register(EmployeeProject)
admin.site.register(EmployeeSkillLevel)
admin.site.register(EmployeeSubSkillLevel)
