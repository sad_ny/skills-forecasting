from django.db import models
from django.db.models import CASCADE
from django.utils.timezone import now

from login.models import User
from skills.models import Position, SubPosition, SkillLevel, SubSkillLevel, SubSkill


EMPLOYEE_ROLES = (
    ("emp", "Сотрудник"),
    ("lid", "Менеджер"),
    ("lol", "Высшее руководство компании"),
    ("com", "Работник комиссии"),
    ("lom", "Менеджер-работник комиссии")
)
DEFAULT_EMPLOYEE_ROLE = EMPLOYEE_ROLES[0][0]

JOB_LEVELS = (
    ('1', 'Junior'),
    ('2', 'Middle'),
    ('3', 'Senior')
)
DEFAULT_JOB_LEVEL = JOB_LEVELS[0][0]


class Job(models.Model):
    position = models.CharField(max_length=64)

    class Meta:
        verbose_name = 'Работа'
        verbose_name_plural = 'Работы'

    def __str__(self):
        return self.position


class Department(models.Model):
    name = models.CharField(max_length=64)

    class Meta:
        verbose_name = 'Бизнес-центр'
        verbose_name_plural = 'Бизнес-центры'

    def __str__(self):
        return self.name


class Team(models.Model):
    department = models.ForeignKey(Department, on_delete=CASCADE, related_name='teams')
    name = models.CharField(max_length=64)

    class Meta:
        verbose_name = 'Команда'
        verbose_name_plural = 'Команды'

    def __str__(self):
        return self.name


class Project(models.Model):
    team = models.ForeignKey(Team, on_delete=CASCADE, related_name='projects')
    name = models.CharField(max_length=64)
    start_date = models.DateField(default=now)
    end_date = models.DateField(default=now)
    actual = models.BooleanField(default=True)
    extra_info = models.TextField()

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def __str__(self):
        return self.name


class ProjectSubSkill(models.Model):
    project = models.ForeignKey(Project, on_delete=CASCADE, related_name='technologies')
    sub_skill = models.ForeignKey(SubSkill, on_delete=CASCADE, related_name='projects')

    class Meta:
        verbose_name = 'Проект - технология'
        verbose_name_plural = 'Связь между проектом и технологией'

    def __str__(self):
        return f"{self.project} - {self.sub_skill}"


class Division(models.Model):
    department = models.ForeignKey(Department, on_delete=CASCADE, related_name='divisions')
    name = models.CharField(max_length=64)

    class Meta:
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'

    def __str__(self):
        return self.name


class Employee(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE, related_name='employee_profiles')
    job = models.ForeignKey(Job, on_delete=CASCADE, related_name='employees')
    team = models.ForeignKey(Team, on_delete=CASCADE, related_name='employees', null=True, blank=True)
    division = models.ForeignKey(Division, on_delete=CASCADE, related_name='employees', null=True, blank=True)
    department = models.ForeignKey(Department, on_delete=CASCADE, related_name='employees', null=True, blank=True)
    start_date = models.DateField(default=now)
    actual = models.BooleanField(default=True)
    level = models.CharField(max_length=3, choices=JOB_LEVELS, default=DEFAULT_JOB_LEVEL)

    # Поля для должности и специализации
    position = models.ForeignKey(Position, on_delete=CASCADE, related_name='employees')
    sub_position = models.ForeignKey(SubPosition, on_delete=CASCADE, related_name='employees')

    # Роль
    role = models.CharField(max_length=3, choices=EMPLOYEE_ROLES, default=DEFAULT_EMPLOYEE_ROLE)

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    def __str__(self):
        return f"{self.user} - {self.sub_position}"


class EmployeeProject(models.Model):
    employee = models.ForeignKey(Employee, on_delete=CASCADE, related_name='employee_projects')
    project = models.ForeignKey(Project, on_delete=CASCADE, related_name='projects_staff')

    class Meta:
        verbose_name = 'Сотрудник - проект'
        verbose_name_plural = 'Связь между сотрудником и проектом'

    def __str__(self):
        return f"{self.employee} - {self.project}"


class EmployeeSkillLevel(models.Model):
    employee = models.ForeignKey(Employee, on_delete=CASCADE, related_name='skill_levels')
    skill_level = models.ForeignKey(SkillLevel, on_delete=CASCADE, related_name='employees')
    datetime = models.DateTimeField(default=now)

    class Meta:
        verbose_name = 'Сотрудник - общий навык'
        verbose_name_plural = 'Связь между сотрудником и общим навыком'

    def __str__(self):
        return f"{self.employee} - {self.skill_level}"


class EmployeeSubSkillLevel(models.Model):
    employee = models.ForeignKey(Employee, on_delete=CASCADE, related_name='sub_skill_levels')
    sub_skill_level = models.ForeignKey(SubSkillLevel, on_delete=CASCADE, related_name='employees')
    datetime = models.DateTimeField(default=now)

    class Meta:
        verbose_name = 'Сотрудник - навык специализации'
        verbose_name_plural = 'Связь между сотрудником и навыком специализации'

    def __str__(self):
        return f"{self.employee} - {self.sub_skill_level}"
