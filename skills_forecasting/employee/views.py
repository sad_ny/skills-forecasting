from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect

from employee.dao import employee_dao
from employee import helpers as employee_helpers
from employee.helpers import editor

from skills.dao import skill_dao
from skills import helpers as skills_helpers

from login.dao import user_dao
from login import helpers as user_helpers
from skills.dao.skill_dao import get_recommendations

from django.http import HttpResponse

from .models import *


@login_required(login_url="login")
def profile_view(request, id):
    auth_user = request.user
    user = user_dao.get_user_by_id(id)
    is_owner = False
    context = locals()
    is_lofl = employee_helpers.is_lofl(auth_user)
    auth_is_in_comission = employee_helpers.is_in_comission(auth_user)
    if auth_user == user or not user.closed or auth_is_in_comission:
        birth_day = user_helpers.get_user_birthday(user)
        age = employee_helpers.convert_age_to_string(employee_helpers.calculate_age(user.birth_date))
        experience = employee_helpers.calculate_experience(user.created)
        experiences = {}
        employees = employee_dao.get_employees_by_user(user)
        employee = employees.first()
        projects = employee_dao.get_employee_projects(employee)
        actual_projects, not_actual_projects = employee_dao.get_project_by_relevance(employee)
        employee_skill_recommendation_levels, employee_subskill_recommendation_levels = skill_dao.get_recommendations(
            employee)

        skills = skill_dao.get_employee_skills(employee)
        subskills = skill_dao.get_employee_subskills(employee)
        softskills = skill_dao.get_employee_softskills(employee)

        departments = Department.objects.all()

        experiences[employee.start_date] = user_helpers.convert_to_CIS_date_format(employee.start_date)
        is_owner = True if auth_user == user else False
        is_in_comission = employee_helpers.is_in_comission(user)
        allowed_to_add_skills = employee_helpers.allowed_to_add_skills(request)
        allowed_to_add_to_team = is_lead(auth_user)
        projects_of_lead = employee_helpers.get_projects_of_lead(auth_user)
        context.update(locals())

    return render(request, "employee/profile.html", context)


@editor
def edit_view(request, id, tab):
    if request.method == 'GET':
        section = None
        if request.GET:
            skill_name = request.GET['skillName']
            level = request.GET['level']
            if skill_name and level:
                skill, section = skill_dao.get_skill_and_section(tab, skill_name)
        if section:
            skills = skill_dao.get_skills(tab)
        sections = skill_dao.get_sections(tab)
        return render(request, 'employee/edit_skill.html', locals())

    elif request.method == 'POST':
        skill_id = request.POST.get('skill')
        level = request.POST.get("inlineDefaultRadiosExample")

        employee_dao.save_employee_skill(skill_id, level, tab, id, request.user)

        return redirect('profile', id=employee_dao.get_employee_by_id(id).user.id)


def edit_skill_data_view(request, id, tab):
    if request.method == 'GET':
        section_id = request.GET.get('section')
        skills = serializers.serialize('json', skill_dao.get_skills_by_section(tab, section_id),
                                       ensure_ascii=False)
        return JsonResponse({"skills": skills})


@editor
def make_comission_worker_view(request, id):
    user = User.objects.get(id=id)
    employees = employee_dao.get_employees_by_user(user)
    for employee in employees:
        if employee.role == 'lid':
            employee.role = 'lom'
        else:
            employee.role = "com"
        employee.save()
    return redirect('profile', id=user.id)

@editor
def remove_from_comission_view(request, id):
    user = User.objects.get(id=id)
    employees = employee_dao.get_employees_by_user(user)
    for employee in employees:
        if employee.role == "com":
            employee.role="emp"
        elif employee.role == 'lom':
            employee.role = 'lid'
        employee.save()
    return redirect('profile', id=user.id)

def personal_recommendation_view(request, id):
    if request.method == 'GET':
        user = get_object_or_404(User, pk=id)
        employees, employee = get_employee(request, user)
        rec_sl, rec_ssl = skill_dao.get_recommendations(user)

        '''
        1) Взять все саб_скилл_левел и скилл_левел, у которых эмплои = вот этот вот
        2) рассматривать веса этих скиллов. Найти минимальный, взять все (саб)скилл_левела с
        минимальным весом
        3) взять все (саб)скилл_левела с весом на единицу больше, выкидываем все скиллы,
        которые у нас уже есть с весом на единицу(как минимум) больше
        '''

        return render(request, "recommendations/recommendations_page.html",
                      {'employees': employees, 'employee': employee,
                       'employee_skill_recommendation_levels': rec_sl,
                       'employee_subskill_recommendation_levels': rec_ssl})


def employee_data_view(request, id):
    if request.method == 'GET':
        employee = get_object_or_404(Employee, pk=id)
        projects = employee_dao.get_employee_projects(employee)
        skills = skills_helpers.convert_skills_to_json(employee, 'skill')
        subskills = skills_helpers.convert_skills_to_json(employee, 'subskill')
        softskills = skills_helpers.convert_skills_to_json(employee, 'softskill')
        employee_skill_recommendation_levels, employee_subskill_recommendation_levels = skill_dao.get_recommendations(
            employee)
        employee_skill_recommendation_levels = skills_helpers.convert_recommend_to_json(employee_skill_recommendation_levels)
        employee_subskill_recommendation_levels = skills_helpers.convert_recommend_to_json(employee_subskill_recommendation_levels)

        projects = serializers.serialize('json', projects)

        return JsonResponse({"projects": projects,
                             "skills": skills,
                             "subskills": subskills,
                             "softskills": softskills,
                             "emplSkillsRecommend": employee_skill_recommendation_levels,
                             "emplSubskillsRecommend": employee_subskill_recommendation_levels,
                             "employee": employee.id})


def change_profile_access(request, id):
    if request.method == 'POST':
        user_dao.set_profile_access(
            user_dao.get_user_by_id(id),
            request.POST.get("closed"))
    return redirect('profile', id=id)


def get_employee(request, user):
    employees = Employee.objects.filter(user=user).order_by('start_date').only('job', 'id')
    emp_id = request.GET.get('emp_id', None)
    if emp_id is None:
        employee = employees[0]
    else:
        # todo: бросать экспшен если не существует такого сотрудника
        employee = employees.get(pk=emp_id)

    return employees, employee


def employee_search_by_name_view(request):
    string = request.GET.get('string')
    no_team = request.GET.get('no_team')
    query = Employee.objects.all()
    if no_team is not None:
        query = query.filter(team_id__isnull=True)
    if string is not None:
        q1 = Q(user__first_name__contains=string)
        q2 = Q(user__last_name__contains=string)
        q3 = Q(user__middle_name__contains=string)
        query = query.filter(Q(q1 | q2 | q3))

    query = serializers.serialize('json', query)
    return JsonResponse({'query': query})


def is_lead(user):
    e = Employee.objects.filter(user=user).filter(role=EMPLOYEE_ROLES[1][0])
    return e.first() is not None


def lead_team_management_view(request, id):
    if is_lead(request.user):
        if request.method == 'POST':
            to_team = request.POST.get('to_team')
            to_project = request.POST.get('to_project')
            project = Project.objects.get(pk=request.POST.get('project_id'))
            team = project.team
            new_employee = Employee.objects.get(pk=request.POST.get('employee_id'))
            if to_team:
                if new_employee is not None:
                    new_employee.team = team
                    new_employee.save()
            if to_project:
                if new_employee is not None:
                    employee_project = EmployeeProject.objects.create(
                        employee=new_employee,
                        project=project
                    )
                    employee_project.save()
            return redirect('profile', id=employee_dao.get_employee_by_id(id).user.id)


def search_by_skills_view(request):
    query_string = request.GET.get('query_string', '')
    results = []
    skip = False

    # Проверяем рекомендации каждого сотрудника
    for employee in Employee.objects.all():
        skill_rec_l, subskill_rec_l = get_recommendations(employee)
        for srl in skill_rec_l:
            # Если скилл совпадает с поиском, добавляем сотрудника в результат, пропускаем проверку саб скиллов
            if srl.skill.name.find(query_string) > -1:
                results.append(employee)
                skip = True
                break

        if skip:
            skip = False
            continue

        # Если скилл совпадает с поиском, добавляем сотрудника в результат
        for ssrl in subskill_rec_l:
            if ssrl.sub_skill.name.find(query_string) > -1:
                results.append(employee)
                break

    results = serializers.serialize('json', results)
    return JsonResponse({'results': results})
