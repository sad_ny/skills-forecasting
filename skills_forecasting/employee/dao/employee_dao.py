from employee.models import *
from skills.models import Skill, UserSoftSkill, SoftSkill
from skills.dao.skill_dao import get_recommendations


def get_employees_by_user(user):
    return Employee.objects.filter(user=user)


def get_employee_by_id(id):
    return Employee.objects.get(pk=id)


def get_projects_by_team(employee):
    return list(Project.objects.filter(team=employee.team))


def get_hired_employee_projects(employee):
    return list(map(lambda empl: empl.project, list(employee.employee_projects.all())))


def get_employee_projects(employee):
    return get_projects_by_team(employee) + get_hired_employee_projects(employee)


def get_project_by_relevance(employee):
    all_projects = get_employee_projects(employee)
    actual = []
    not_actual = []
    for project in all_projects:
        if project.actual:
            actual.append(project)
        else:
            not_actual.append(project)
    return actual, not_actual


def save_employee_skill(skill_id, level, tab, id, cur_user):
    employee = get_employee_by_id(id)
    if tab == 1:
        skill_levels = SkillLevel.objects.filter(
            skill=Skill.objects.get(id=skill_id))
        if skill_levels.exists():
            employees_skill_level = []
            for skill_level in skill_levels:
                employee_skill_level = EmployeeSkillLevel.objects.filter(employee=employee,
                                                                         skill_level=skill_level)
                if employee_skill_level.exists():
                    employees_skill_level.append(employee_skill_level.last())
            if len(employees_skill_level) > 0:
                employee_skill_level = employees_skill_level[-1]
            else:
                employee_skill_level = EmployeeSkillLevel()
            employee_skill_level.employee = employee
            skill_level = SkillLevel.objects.filter(
                skill=Skill.objects.get(id=skill_id),
                weight=level)
            if skill_level.exists():
                employee_skill_level.skill_level = skill_level.get()
                employee_skill_level.save()

    elif tab == 2:
        sub_skill_levels = SubSkillLevel.objects.filter(
            sub_skill=SubSkill.objects.get(id=skill_id))
        if sub_skill_levels.exists():
            employees_subskill_level = []
            for sub_skill_level in sub_skill_levels:
                employee_subskill_level = EmployeeSubSkillLevel.objects.filter(employee=employee,
                                                                               sub_skill_level=sub_skill_level)
                if employee_subskill_level.exists():
                    employees_subskill_level.append(employee_subskill_level.last())
            if len(employees_subskill_level) > 0:
                employee_subskill_level = employees_subskill_level[-1]
            else:
                employee_subskill_level = EmployeeSubSkillLevel()
            employee_subskill_level.employee = employee
            sub_skill_level = SubSkillLevel.objects.filter(
                sub_skill=SubSkill.objects.get(id=skill_id),
                weight=level)
            if sub_skill_level.exists():
                employee_subskill_level.sub_skill_level = sub_skill_level.get()
                employee_subskill_level.save()
    elif tab == 3:
        soft_skill = SoftSkill.objects.get(id=skill_id)
        employee_soft_skill_level = UserSoftSkill.objects.filter(estimated=employee.user,
                                                                 soft_skill=soft_skill)
        if not employee_soft_skill_level.exists():
            employee_soft_skill_level = UserSoftSkill()
        else:
            employee_soft_skill_level = employee_soft_skill_level.last()
        employee_soft_skill_level.soft_skill = soft_skill
        employee_soft_skill_level.estimated = employee.user
        employee_soft_skill_level.estimator = cur_user
        employee_soft_skill_level.value = level
        employee_soft_skill_level.save()

    # Пересчет уровня сотрудника
    recalculate_employee_level(employee)


def recalculate_employee_level(employee):
    next_level_weight = int(employee.level) + 1

    if has_all_necessary_skills_for_next_level(employee, next_level_weight):
        # сначала max
        level = 3
        # is_the_highest - проверяется, высший ли это уровень навыка
        for skill_level in get_employee_skills_levels(employee):
            if skill_level.weight < level:
                if not is_the_highest(skill_level) or skill_level.weight == next_level_weight:
                    level = skill_level.weight

        employee.level = level
        employee.save()


def has_all_necessary_skills_for_next_level(employee, next_level_weight):
    # Все скиллы по специализации и должности
    position_skills_levels = SkillLevel.objects.filter(skill__position=employee.position, weight=next_level_weight)
    sub_position_sub_skills_levels = SubSkillLevel.objects.filter(sub_skill__sub_position=employee.sub_position,
                                                                  weight=next_level_weight)

    position_skills = set([position_skills_level.skill for position_skills_level in position_skills_levels])
    sub_position_sub_skills = set([sub_position_sub_skills_level.sub_skill for sub_position_sub_skills_level in
                                   sub_position_sub_skills_levels])

    # Скиллы сотрудника
    employee_skills = set(esl.skill_level.skill for esl in EmployeeSkillLevel.objects.filter(employee=employee))
    employee_subskills = set(
        essl.sub_skill_level.sub_skill for essl in EmployeeSubSkillLevel.objects.filter(employee=employee))

    if position_skills.issubset(employee_skills) and sub_position_sub_skills.issubset(employee_subskills):
        return True
    return False


def is_the_highest(skill_level):
    if isinstance(skill_level, SkillLevel):
        skill_levels = SkillLevel.objects.filter(skill=skill_level.skill)
    else:
        skill_levels = SubSkillLevel.objects.filter(sub_skill=skill_level.sub_skill)

    max_weight = 0
    for sl in skill_levels:
        if sl.weight > max_weight:
            max_weight = sl.weight

    if skill_level.weight == max_weight:
        return True
    return False


def get_employee_skills_levels(employee):
    employee_skills = set(esl.skill_level for esl in EmployeeSkillLevel.objects.filter(employee=employee))
    employee_sub_skills = set(essl.sub_skill_level for essl in EmployeeSubSkillLevel.objects.filter(employee=employee))

    return employee_skills.union(employee_sub_skills)


def get_employees_by_skill(skill_type, skill_id):
    if skill_type == "skill":
        skill = Skill.objects.get(id=skill_id)
        employees = [(employee_skill_level.employee, employee_skill_level.skill_level.name) for employee_skill_level in
                     set(EmployeeSkillLevel.objects.filter(skill_level__skill=skill))]
    elif skill_type == "subskill":
        skill = SubSkill.objects.get(id=skill_id)
        employees = [(employee_skill_level.employee, employee_skill_level.sub_skill_level.name) for employee_skill_level in
                     set(EmployeeSubSkillLevel.objects.filter(sub_skill_level__sub_skill=skill))]
    else:
        skill = SoftSkill.objects.get(id=skill_id)
        employees = [(employee_skill_level.employee, employee_skill_level.value) for employee_skill_level in
                     set(UserSoftSkill.objects.filter(soft_skill=skill))]

    return employees, skill


def get_employees_by_skill_in_rec(skill_type, skill_id):
    employees = []
    if skill_type == "skill":
        skill = Skill.objects.get(id=skill_id)
        for employee in Employee.objects.all():
            employee_skill_recommendation_levels, employee_subskill_recommendation_levels = get_recommendations(employee)
            for rec_sl in employee_skill_recommendation_levels:
                if rec_sl.skill == skill:
                    employees.append((employee, rec_sl.name))
    elif skill_type == "subskill":
        skill = SubSkill.objects.get(id=skill_id)
        for employee in Employee.objects.all():
            employee_skill_recommendation_levels, employee_subskill_recommendation_levels = get_recommendations(employee)
            for rec_ssl in employee_subskill_recommendation_levels:
                if rec_ssl.sub_skill == skill:
                    employees.append((employee, rec_ssl.name))
    else:
        skill = SoftSkill.objects.get(id=skill_id)
    return employees, skill
