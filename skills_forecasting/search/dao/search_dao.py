from skills.models import Skill, SubSkill, SoftSkill


def get_skills_by_substring(substring):
    skills = list(Skill.objects.filter(name__contains=substring))
    sub_skills = list(SubSkill.objects.filter(name__contains=substring))
    soft_skills = list(SoftSkill.objects.filter(name__contains=substring))
    return skills + sub_skills + soft_skills
