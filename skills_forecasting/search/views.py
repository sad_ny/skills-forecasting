from django.core import serializers
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

from employee import helpers
from employee.dao import employee_dao
from login.dao import user_dao
from search.dao import search_dao


def search_view(request):
    if request.method == 'GET':
        context = {'users': user_dao.get_users_by_fio_substring(request.GET['search']), 'auth_user': request.user}
        return render(request, 'search/search_results.html', context)


def search_by_skill_view(request):
    auth_user = request.user
    if helpers.is_lofl(auth_user):
        return render(request, 'search/search_by_skill.html', locals())
    return HttpResponse("Вы не можете искать сотрудников по навыкам.")


def search_data_view(request):
    if request.method == 'GET':
        string = request.GET['string']
        skills = serializers.serialize('json', search_dao.get_skills_by_substring(string) if string != "" else [],
                                       ensure_ascii=False)
        return JsonResponse({"skills": skills})


def search_employees_view(request, rec):
    auth_user = request.user
    if helpers.is_lofl(auth_user):
        if request.method == 'GET':
            if (rec == 0):
                employees = None
                skill = None
                auth_user = request.user
                if request.GET:
                    skill_type = request.GET['type']
                    skill_id = request.GET['id']
                    if skill_type and skill_id:
                        employees, skill = employee_dao.get_employees_by_skill(skill_type.split(".")[1], skill_id)
                return render(request, 'search/search_employee_list.html',
                              {'employees': employees, 'auth_user': auth_user, 'skill': skill})
            elif (rec == 1):
                if request.GET:
                    skill_type = request.GET['type']
                    skill_id = request.GET['id']
                    if skill_type and skill_id:
                        employees, skill = employee_dao.get_employees_by_skill_in_rec(skill_type.split(".")[1], skill_id)
                return render(request, 'search/search_employee_list.html',
                              {'employees': employees, 'auth_user': auth_user, 'skill': skill, 'rec': True})
    return HttpResponse("Вы не можете искать сотрудников по навыкам.")
