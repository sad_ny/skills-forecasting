from django.urls import path

from . import views

urlpatterns = [
    path('', views.search_view, name='search'),
    path('by_skill', views.search_by_skill_view, name='search_by_skill'),
    path('by_skill/data', views.search_data_view, name='search_data'),
    path('by_skill/employees/<int:rec>', views.search_employees_view, name='search_employees'),
]
