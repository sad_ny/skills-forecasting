from employee.models import *
from login.models import *
from skills.models import *

from _datetime import datetime

"""fill db"""


def fill_db():
    print('Filling DB...', end=" ")

    polina = User.objects.create_user(username='polya', password='123456', email='polya@mother.com',
                                      first_name='Полина',
                                      middle_name='Дмитриевна',
                                      last_name='Аликина',
                                      birth_date=datetime.strptime('1999-09-27', "%Y-%m-%d").date(),
                                      closed=False)

    anya = User.objects.create_user(username='anya', password='123456', email='anya@sister.com', first_name='Анна',
                                    middle_name='Александровна',
                                    last_name='Савельева',
                                    birth_date=datetime.strptime('1999-12-30', "%Y-%m-%d").date(),
                                    closed=True)

    dilyara = User.objects.create_user(username='dilyara', password='123456', email='dilyara@sister.com',
                                       first_name='Диляра',
                                       middle_name='Рависовна',
                                       last_name='Мухамедшина',
                                       birth_date=datetime.strptime('1999-09-01', "%Y-%m-%d").date(), closed=False)

    andrey = User.objects.create_user(username='andrey', password='123456', email='andrey@brother.com',
                                      first_name='Андрей',
                                      middle_name='Сергеевич',
                                      last_name='Саховский',
                                      birth_date=datetime.strptime('1999-01-29', "%Y-%m-%d").date(), closed=False)

    evelina = User.objects.create_user(username='evelina', password='123456', email='evelina@super.com',
                                       first_name='Эвелина',
                                       middle_name='Фанисовна',
                                       last_name='Равилова',
                                       birth_date=datetime.strptime('1999-07-28', "%Y-%m-%d").date(), closed=True)

    nikita = User.objects.create_user(username='nikita', password='123456', email='nikita@puper.com',
                                       first_name='Никита',
                                       middle_name='Витальевич',
                                       last_name='Ясавиев',
                                       birth_date=datetime.strptime('1998-10-06', "%Y-%m-%d").date(), closed=True)

    housing = Department.objects.create(name='ЖКХ')
    medicine = Department.objects.create(name='Медицина')

    housing_dev = Division.objects.create(department=housing, name='Отдел разработчиков')
    housing_analytics = Division.objects.create(department=housing, name='Отдел аналитиков')
    medicine_test = Division.objects.create(department=medicine, name='Отдел тестировщиков')
    medicine_dev = Division.objects.create(department=medicine, name='Отдел разработчиков')
    medicine_front = Division.objects.create(department=medicine, name='Отдел верстальщиков')

    pm = Job.objects.create(position='Менеджер проектов')
    main_dev = Job.objects.create(position='Главный разработчик')
    front = Job.objects.create(position='Верстальщик')
    qa = Job.objects.create(position='Тестировщик ПО')
    front_html = Job.objects.create(position='HTML верстальщик')
    python_senior = Job.objects.create(position='Python Senior Developer')
    python_middle = Job.objects.create(position='Python Middle Developer')
    python_junior = Job.objects.create(position='Python Junior Developer')
    header = Job.objects.create(position='Управляющий')

    sweep = Team.objects.create(department=housing, name='Трубочисты')
    med_emergency = Team.objects.create(department=medicine, name='Спасатели врачей')
    dev_emergency = Team.objects.create(department=medicine, name='Больница скорой программистской помощи')

    back_dev = Position.objects.create(name='Backend-разработчик')
    front_dev = Position.objects.create(name='Frontend-разработчик')
    lead = Position.objects.create(name='Менеджер')
    test = Position.objects.create(name='Тестировщик')
    chief = Position.objects.create(name='Руководитель')

    back_dev_python = SubPosition.objects.create(position=back_dev, name='Python разработчик')
    front_dev_html = SubPosition.objects.create(position=front_dev, name='HTML разработчик')
    back_dev_java = SubPosition.objects.create(position=back_dev, name='Java разработчик')
    team_lead = SubPosition.objects.create(position=lead, name='Руководитель команды')
    project_lead = SubPosition.objects.create(position=lead, name='Руководитель проекта')
    test_manual = SubPosition.objects.create(position=test, name='Специалист по ручному тестированию')
    test_auto = SubPosition.objects.create(position=test, name='Специалист по автоматизированному тестированию')
    test_lead = SubPosition.objects.create(position=test, name='Руководитель отдела тестирования')
    dev_chief = SubPosition.objects.create(position=chief, name='Руководитель отдела разработки')

    anya_empl = Employee.objects.create(user=anya, job=python_junior, team=sweep, division=housing_dev,
                                        department=housing,
                                        position=back_dev,
                                        sub_position=back_dev_python, role='com', level='1')
    polya_empl = Employee.objects.create(user=polina, job=front_html, team=dev_emergency, division=medicine_front,
                                         department=medicine,
                                         position=front_dev,
                                         sub_position=front_dev_html)
    polya_emp2 = Employee.objects.create(user=polina, job=pm, team=sweep,
                                         department=housing,
                                         position=lead,
                                         sub_position=team_lead, role='lid')
    andrey_empl = Employee.objects.create(user=andrey, job=python_middle, team=dev_emergency, division=medicine_dev,
                                          department=medicine,
                                          position=back_dev,
                                          sub_position=back_dev_python, level='2')
    andrey_emp2 = Employee.objects.create(user=andrey, job=main_dev, department=medicine, team=med_emergency,
                                          position=back_dev,
                                          sub_position=back_dev_java)
    andrey_emp3 = Employee.objects.create(user=andrey, job=qa, department=medicine, division=medicine_test,
                                          position=test,
                                          sub_position=test_manual)
    evelina_empl = Employee.objects.create(user=evelina, job=python_senior, department=housing,
                                           position=back_dev,
                                           sub_position=back_dev_python, level='3')
    evelina_emp2 = Employee.objects.create(user=evelina, job=header, position=chief,
                                           sub_position=dev_chief, role='lol')
    dilyara_empl = Employee.objects.create(user=dilyara, job=qa, department=medicine, division=medicine_test,
                                           position=test, sub_position=test_auto)
    nikita_empl = Employee.objects.create(user=nikita, job=main_dev, department=housing,
                                           position=back_dev,
                                           sub_position=back_dev_java, level='3')
    nikita_emp2 = Employee.objects.create(user=nikita, job=qa, department=medicine, division=medicine_test,
                                          team=dev_emergency,
                                          position=test,
                                          sub_position=test_lead, level='3')

    dbms = Skill.objects.create(position=back_dev, name='СУБД')
    algorithms = Skill.objects.create(position=back_dev, name='Алгоритмы и структуры данных')
    design_IS = Skill.objects.create(position=back_dev, name='Навыки проектирования информационных систем')
    git = Skill.objects.create(position=front_dev, name="Работа с Git")
    english = Skill.objects.create(position=lead, name="Английский язык")
    technologies = Skill.objects.create(position=back_dev, name='Знание технологий и платформ')
    instruments = Skill.objects.create(position=back_dev, name='Инструменты разработки')

    dbms_lvl_1 = SkillLevel.objects.create(skill=dbms, name='Junior level',
                                           description='Умение работать с РСУБД на базовом уровне (базовые DDL, '
                                                       'DML операции.)',
                                           weight=1)
    dbms_lvl_2 = SkillLevel.objects.create(skill=dbms, name='Middle level',
                                           description='Понимание ACID, транзакций. Умение оценить сложность запросов '
                                                       'на базе плана выполнения, построить необходимые индексы. '
                                                       'Способность спроектировать файловое хранилище для приложения.',
                                           weight=2)
    dbms_lvl_3 = SkillLevel.objects.create(skill=dbms, name='Senior level',
                                           description='Способность спроектировать хорошие нормализованные схемы БД, '
                                                       'с учетом запросов, которые будут выполняться. Умение применять '
                                                       'представления, хранимые процедуры, триггеры и собственные '
                                                       'типы данных. Понимание разницы между кластеризованными и '
                                                       'не-кластеризованными индексами. Опыт работы с '
                                                       'нереляционными СУБД.',
                                           weight=3)
    algorithms_lvl_1 = SkillLevel.objects.create(skill=algorithms, name='Junior level',
                                                 description='Умение объяснить и использовать на практике массивы, связные '
                                                             'списки, словари и т.д. Способность использовать базовые алгоритмы '
                                                             'работы с этими структурами (поиск, обход, сортировка).',
                                                 weight=1)
    algorithms_lvl_2 = SkillLevel.objects.create(skill=algorithms, name='Middle level',
                                                 description='Понимание и способность выбирать структуры данных и алгоритм под '
                                                             'задачу (временная и пространственная сложность алгоритма). '
                                                             'Знание особенностей и умение применять графы( деревья), хеш-таблицы.',
                                                 weight=2)
    algorithms_lvl_3 = SkillLevel.objects.create(skill=algorithms, name='Senior level',
                                                 description='Знание сложных структур данных (B-дерево, Биномиальная куча'
                                                             ' и Фибоначчиевская куча, АВЛ-дерево, Красно-чёрное дерево, '
                                                             'Косое дерево, Список с пропусками, TRIE-структуры и т.д.).',
                                                 weight=3)
    design_IS_lvl_1 = SkillLevel.objects.create(skill=design_IS, name='Junior level',
                                                description='Способность создать программу/систему по предлагаемым шаблонам/гайдам.'
                                                            ' Знание ООП, DRY на уровне статьей в Википедии.',
                                                weight=1)
    design_IS_lvl_2 = SkillLevel.objects.create(skill=design_IS, name='Middle level',
                                                description='Отличное владение ООП, способность свободно рассуждать о плюсах и минусах.'
                                                            ' Владение основными шаблонами проектирования, умение их распознать.'
                                                            ' Проектирование несложных ненагруженных систем,'
                                                            ' способных стабильно работать и развиваться.',
                                                weight=2)
    design_IS_lvl_3 = SkillLevel.objects.create(skill=design_IS, name='Senior level',
                                                description='Владение навыками проектирования информационных систем, '
                                                            'выходя за пределы шаблонов. Понимание применимости шаблонов,'
                                                            ' способность обосновать их использование/неиспользование.'
                                                            ' Знание UML на уровне, позволяющем создавать '
                                                            'модели прикладных систем.',
                                                weight=3)
    git_lvl_1 = SkillLevel.objects.create(skill=git, name='Junior level', description='Основы Git.', weight=1)
    git_lvl_2 = SkillLevel.objects.create(skill=git, name='Middle level',
                                          description='Ветвление в Git, Распределённый Git.', weight=2)
    git_lvl_3 = SkillLevel.objects.create(skill=git, name='Senior level',
                                          description='Перезапись истории, Git изнутри.', weight=3)
    english_lvl_1 = SkillLevel.objects.create(skill=english, name='Intermediate', weight=1)
    english_lvl_2 = SkillLevel.objects.create(skill=english, name='Upper-Intermediate', weight=2)
    english_lvl_3 = SkillLevel.objects.create(skill=english, name='Advanced', weight=3)

    technologies_lvl_1 = SkillLevel.objects.create(skill=technologies, name='Junior level', weight=1,
                                                   description='Знание на начальном уровне требуемый стек технологий и фреймворки.'
                                                               ' Ознакомленность (возможно без личного опыта) с наиболее популярными '
                                                               'компонентами/фреймворками в своем стеке технологий.')
    technologies_lvl_2 = SkillLevel.objects.create(skill=technologies, name='Middle level', weight=2,
                                                   description='Знание альтернативных технологий, сильных/слабых сторон.'
                                                               ' Уверенное владение основными используемыми фреймворками/'
                                                               'компонентами, пробы других, знание их особенностей. '
                                                               'Опыт работы с нагруженными системами.'
                                                               ' Понимание процесса сборки приложения.'
                                                               ' Понимание процесса исполнения приложения, '
                                                               'управление памятью, обращение к ресурсам среды исполнения.')
    technologies_lvl_3 = SkillLevel.objects.create(skill=technologies, name='Senior level', weight=3,
                                                   description='Понимание работы всего "программного стэка":'
                                                               ' железо/среда исполнения, компиляция(jit)/линковка/'
                                                               'интерпретация, управление памятью(сборка мусора, куча,'
                                                               ' стэк, адресация памяти). Понимание работы '
                                                               'с многопоточностью, синхронизацией потоков.'
                                                               ' Написание компонентов/фреймворков, используемых в компании/'
                                                               'сообществе.')
    instruments_lvl_1 = SkillLevel.objects.create(skill=instruments, name='Junior level', weight=1,
                                                  description='Владение базисом инструмента разработки/'
                                                              'авто-тестирования, работа с VCS,'
                                                              ' запуск сборки в CI.')
    instruments_lvl_2 = SkillLevel.objects.create(skill=instruments, name='Middle level', weight=2,
                                                  description='Полное владение инструментами разработки, '
                                                              'принятыми в производственном подразделении, '
                                                              'в том числе IDE+расширения '
                                                              '(в том числе инструменты профилирования,'
                                                              ' декомпилирования исходных кодов, автотестов),'
                                                              ' VCS (понимание принципов работы, '
                                                              'ветвления, хранения исходников),'
                                                              ' CI (способность настраивать сборку).')
    instruments_lvl_3 = SkillLevel.objects.create(skill=instruments, name='Senior level', weight=3,
                                                  description='Использование набора инструментов, '
                                                              'позволяющего максимально эффективно решать '
                                                              'задачи (включая нетиповые). '
                                                              'Обоснование выбора этого набора. '
                                                              'Использование и создание расширения инструментов.')

    anya_empl_dbms_2 = EmployeeSkillLevel.objects.create(employee=anya_empl, skill_level=dbms_lvl_2)
    anya_empl_design_IS_1 = EmployeeSkillLevel.objects.create(employee=anya_empl, skill_level=design_IS_lvl_1)

    andrey_empl_dbms_2 = EmployeeSkillLevel.objects.create(employee=andrey_empl, skill_level=dbms_lvl_2)
    andrey_empl_design_IS_2 = EmployeeSkillLevel.objects.create(employee=andrey_empl, skill_level=design_IS_lvl_2)

    polya_empl_git_2 = EmployeeSkillLevel.objects.create(employee=polya_empl, skill_level=git_lvl_2)
    polya_emp2_eng = EmployeeSkillLevel.objects.create(employee=polya_emp2, skill_level=english_lvl_2)

    evelina_empl_dbms_lvl_3 = EmployeeSkillLevel.objects.create(employee=evelina_empl, skill_level=dbms_lvl_3)
    evelina_empl_design_IS_3 = EmployeeSkillLevel.objects.create(employee=evelina_empl, skill_level=design_IS_lvl_3)
    evelina_empl_git_3 = EmployeeSkillLevel.objects.create(employee=evelina_empl, skill_level=git_lvl_3)
    evelina_empl_alg_3 = EmployeeSkillLevel.objects.create(employee=evelina_empl, skill_level=algorithms_lvl_3)
    evelina_empl_technologies_3 = EmployeeSkillLevel.objects.create(employee=evelina_empl,
                                                                    skill_level=technologies_lvl_3)
    evelina_empl_instruments_3 = EmployeeSkillLevel.objects.create(employee=evelina_empl, skill_level=instruments_lvl_3)
    nikita_empl_dbms_lvl_3 = EmployeeSkillLevel.objects.create(employee=nikita_empl, skill_level=dbms_lvl_3)
    nikita_empl_design_IS_3 = EmployeeSkillLevel.objects.create(employee=nikita_empl, skill_level=design_IS_lvl_3)
    nikita_empl_git_3 = EmployeeSkillLevel.objects.create(employee=nikita_empl, skill_level=git_lvl_3)
    nikita_empl_alg_3 = EmployeeSkillLevel.objects.create(employee=nikita_empl, skill_level=algorithms_lvl_3)
    nikita_empl_technologies_3 = EmployeeSkillLevel.objects.create(employee=nikita_empl,
                                                                    skill_level=technologies_lvl_3)
    nikita_empl_instruments_3 = EmployeeSkillLevel.objects.create(employee=nikita_empl, skill_level=instruments_lvl_3)

    python_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='Python')
    django_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='Django')
    css_front = SubSkill.objects.create(sub_position=front_dev_html, name='CSS')
    vue_front = SubSkill.objects.create(sub_position=front_dev_html, name='Vue.js')
    time_management = SubSkill.objects.create(sub_position=team_lead, name="Тайм-менеджмент")
    web_protocols_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='Веб-протоколы')
    cryptography_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='Криптография')
    sql_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='SQL (в postgres)')
    m3_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='M3')
    js_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='Javascript (ExtJS)')
    deploy_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='Deploy (gunicorn/uwsgi/nginx)')
    deploy_back_dev_2 = SubSkill.objects.create(sub_position=back_dev_python, name='Deploy (postgres)')
    ide_back_dev = SubSkill.objects.create(sub_position=back_dev_python, name='IDE')
    java_back_dev = SubSkill.objects.create(sub_position=back_dev_java, name='Java')

    python_back_dev_lvl_1 = SubSkillLevel.objects.create(sub_skill=python_back_dev, name='Junior level',
                                                         description='Практическое введение в Python для Django"'
                                                                     ' (книга "Django. Разработка веб-приложений на Python",'
                                                                     ' Джефф Форсье, Пол Биссекс, Уэсли Чан).',
                                                         weight=1)
    python_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=python_back_dev, name='Middle level',
                                                         description='Глава "I. Язык программирования Python" от и до,'
                                                                     ' разделы 12-15 из II главы (книга "Python. Подробный'
                                                                     ' справочник" Автор: David Beazley).',
                                                         weight=2)
    python_back_dev_lvl_3 = SubSkillLevel.objects.create(sub_skill=python_back_dev, name='Senior level',
                                                         description='Потоки и многозадачность, Работа с сетью и сокеты.'
                                                                     ' Разделы 16-25 из II главы (книга "Python. Подробный'
                                                                     ' справочник" Автор: David Beazley).',
                                                         weight=3)
    django_back_dev_lvl_1 = SubSkillLevel.objects.create(sub_skill=django_back_dev, name='Junior level',
                                                         description='"Первые шаги" (документация Django).',
                                                         weight=1)
    django_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=django_back_dev, name='Middle level',
                                                         description='Официальная документация к Django "от и до".',
                                                         weight=2)
    css_front_lvl_1 = SubSkillLevel.objects.create(sub_skill=css_front, name=' Junior level', description='Forms.',
                                                   weight=1)
    css_front_lvl_2 = SubSkillLevel.objects.create(sub_skill=css_front, name='Middle level', description='Pages.',
                                                   weight=2)
    time_management_lvl_1 = SubSkillLevel.objects.create(sub_skill=time_management, name="Agile",
                                                         description='Основы Agile, SCRUM & Kanban',
                                                         weight=1)
    web_protocols_back_dev_lvl_1 = SubSkillLevel.objects.create(sub_skill=web_protocols_back_dev, name="Junior level",
                                                                description='HTTP',
                                                                weight=1)
    web_protocols_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=web_protocols_back_dev, name="Middle level",
                                                                description='SOAP/WSDL, REST, Websocket',
                                                                weight=2)
    cryptography_back_dev_lvl_3_n_only = SubSkillLevel.objects.create(sub_skill=cryptography_back_dev,
                                                                      name="Senior level",
                                                                      description='Открытый/закрытый ключ/'
                                                                                  'Цифровая подпись/Сертификат',
                                                                      weight=3)
    sql_back_dev_lvl_1 = SubSkillLevel.objects.create(sub_skill=sql_back_dev, name="Junior level",
                                                      description='SQL Syntax , Data Definition, Data Manipulation',
                                                      weight=1)
    sql_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=sql_back_dev, name="Middle level",
                                                      description='Queries, Data Types, Functions and Operators,'
                                                                  ' Type Conversion, Full Text Search, Indexes, '
                                                                  'Performance Tips',
                                                      weight=2)
    sql_back_dev_lvl_3 = SubSkillLevel.objects.create(sub_skill=sql_back_dev, name="Senior level",
                                                      description='Теоретические знания о Triggers, '
                                                                  'The Rule System, Procedural Languages '
                                                                  '- что это и в каких случаях может быть применено.'
                                                                  ' Может при необходимости быстро изучить и '
                                                                  'применить на практике.', weight=3)
    m3_back_dev_lvl_1 = SubSkillLevel.objects.create(sub_skill=m3_back_dev, name="Junior level",
                                                     description='Может запустить новый проект на m3-blank',
                                                     weight=1)
    m3_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=m3_back_dev, name="Middle level",
                                                     description='url dispatcher, Action/ActionPack, '
                                                                 'базовые модели, миддлеваре, objectpack/recordpack. '
                                                                 'Знает основные "батарейки" m3 и их назначение.'
                                                                 ' Принятые PR в основные модули - плюс.',
                                                     weight=2)
    m3_back_dev_lvl_3 = SubSkillLevel.objects.create(sub_skill=m3_back_dev, name="Senior level",
                                                     description='Участие в развитии существующих модулей/'
                                                                 'реализация новых модулей.',
                                                     weight=3)
    js_back_dev_lvl_1 = SubSkillLevel.objects.create(sub_skill=js_back_dev, name="Junior level",
                                                     description='Способность написать простой обработчик полей, '
                                                                 'наример активация/деактивация поля по условию.'
                                                                 ' Ext Layout - верстка форм и компонент.',
                                                     weight=1)
    js_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=js_back_dev, name="Middle level",
                                                     description='Базовые классы Ext.'
                                                                 ' Базовые знания языка (типы данных, замыкания).',
                                                     weight=2)
    deploy_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=deploy_back_dev, name="Middle level",
                                                         description='Setuptools, pypi, wheels, sdist. '
                                                                     'Типовая инструкция по установке приложения на '
                                                                     'linux и gunicorn.',
                                                         weight=2)
    deploy_back_dev_lvl_3 = SubSkillLevel.objects.create(sub_skill=deploy_back_dev, name="Senior level",
                                                         description='Конфигурация и особенности gunicorn, '
                                                                     'rabbitMq, redis, memcache. '
                                                                     'Методы и инструменты мониторинга и '
                                                                     'анализа работы веб-приложения.',
                                                         weight=3)
    deploy_back_dev_2_lvl_3 = SubSkillLevel.objects.create(sub_skill=deploy_back_dev_2, name="Senior level",
                                                           description='pg_badger, системные представления, '
                                                                       'инструменты мониторинга',
                                                           weight=3)
    ide_back_dev_lvl_2 = SubSkillLevel.objects.create(sub_skill=ide_back_dev, name="Middle level",
                                                      description='Практические вопросы по знанию IDE, '
                                                                  'фичи, ускоряющие работу разработчика',
                                                      weight=2)
    java_back_dev_lvl_3 = SubSkillLevel.objects.create(sub_skill=java_back_dev, name="Senior level",
                                                      description='JAVA skills',
                                                      weight=3)

    anya_empl_python_back_dev_lvl_1 = EmployeeSubSkillLevel.objects.create(employee=anya_empl,
                                                                           sub_skill_level=python_back_dev_lvl_1)
    anya_empl_django_back_dev_lvl_2 = EmployeeSubSkillLevel.objects.create(employee=anya_empl,
                                                                           sub_skill_level=django_back_dev_lvl_2)
    andrey_empl_python_back_dev_lvl_2 = EmployeeSubSkillLevel.objects.create(employee=andrey_empl,
                                                                             sub_skill_level=python_back_dev_lvl_2)
    andrey_empl_django_back_dev_lvl_2 = EmployeeSubSkillLevel.objects.create(employee=andrey_empl,
                                                                             sub_skill_level=django_back_dev_lvl_2)
    polya_empl_css_front_lvl_2 = EmployeeSubSkillLevel.objects.create(employee=polya_empl,
                                                                      sub_skill_level=css_front_lvl_2)
    polya_emp2_managem_lvl_1 = EmployeeSubSkillLevel.objects.create(employee=polya_emp2,
                                                                    sub_skill_level=time_management_lvl_1)
    evelina_empl_django_back_dev_lvl_2 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                              sub_skill_level=django_back_dev_lvl_2)
    evelina_empl_python_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                              sub_skill_level=python_back_dev_lvl_3)
    evelina_empl_web_back_dev_lvl_2 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                           sub_skill_level=web_protocols_back_dev_lvl_2)
    evelina_empl_crypto_back_dev = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                        sub_skill_level=cryptography_back_dev_lvl_3_n_only)
    evelina_empl_sql_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                           sub_skill_level=sql_back_dev_lvl_3)
    evelina_empl_m3_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                          sub_skill_level=m3_back_dev_lvl_3)
    evelina_empl_js_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                          sub_skill_level=js_back_dev_lvl_2)
    evelina_empl_deploy_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                              sub_skill_level=deploy_back_dev_lvl_3)
    evelina_empl_deploy2_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                               sub_skill_level=deploy_back_dev_2_lvl_3)
    evelina_empl_ide_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=evelina_empl,
                                                                           sub_skill_level=ide_back_dev_lvl_2)
    nikita_empl_java_back_dev_lvl_3 = EmployeeSubSkillLevel.objects.create(employee=nikita_empl,
                                                                           sub_skill_level=java_back_dev_lvl_3)

    anti_anti_vax = Project.objects.create(team=dev_emergency, name='Система прививок антипрививочникам',
                                           extra_info='Ввод прививок людям антипрививочникам.')
    housing_bills = Project.objects.create(team=sweep, name='Оплата услуг ЖКХ',
                                           extra_info='Расчет ежемесячной оплаты коммунальных услуг, отопления, создание счетов и их ведение.')
    health_insurance = Project.objects.create(team=dev_emergency, name='Единая система медицинского страхования',
                                              start_date=datetime.strptime('2006-10-25', "%Y-%m-%d").date(),
                                              end_date=datetime.strptime('2018-12-15', "%Y-%m-%d").date(), actual=False,
                                              extra_info='Единая система страхования жителей республики Татарстан.')
    anti_anti_vax_sk1 = ProjectSubSkill.objects.create(project=anti_anti_vax, sub_skill=python_back_dev)
    anti_anti_vax_sk2 = ProjectSubSkill.objects.create(project=anti_anti_vax, sub_skill=django_back_dev)
    anti_anti_vax_sk3 = ProjectSubSkill.objects.create(project=anti_anti_vax, sub_skill=m3_back_dev)

    person_to_project_1 = EmployeeProject.objects.create(employee=evelina_empl, project=health_insurance)
    person_to_project_2 = EmployeeProject.objects.create(employee=anya_empl, project=health_insurance)
    person_to_project_3 = EmployeeProject.objects.create(employee=polya_emp2, project=health_insurance)
    person_to_project_4 = EmployeeProject.objects.create(employee=nikita_empl, project=housing_bills)
    person_to_project_5 = EmployeeProject.objects.create(employee=andrey_emp2, project=anti_anti_vax)
    person_to_project_5_1 = EmployeeProject.objects.create(employee=andrey_emp2, project=health_insurance)
    person_to_project_5_2 = EmployeeProject.objects.create(employee=andrey_emp3, project=housing_bills)
    person_to_project_6 = EmployeeProject.objects.create(employee=evelina_emp2, project=housing_bills)
    person_to_project_7 = EmployeeProject.objects.create(employee=dilyara_empl, project=anti_anti_vax)
    person_to_project_8 = EmployeeProject.objects.create(employee=dilyara_empl, project=housing_bills)
    person_to_project_9 = EmployeeProject.objects.create(employee=dilyara_empl, project=health_insurance)

    # qa skills
    test_instruments = SubSkill.objects.create(sub_position=test_manual,
                                               name='Владение на базовом уровне инструментами тестирования'
                                                    '(SoapUI, testms, консоль браузера)')
    bug_documenting = SubSkill.objects.create(sub_position=test_manual, name='Исследование и документирование багов')
    get_log = SubSkill.objects.create(sub_position=test_manual, name='Умение получения лога ошибки в системе')

    test_documentation = SubSkill.objects.create(sub_position=test_manual,
                                                 name='Составлять тестовую документацию')
    test_types = SubSkill.objects.create(sub_position=test_manual,
                                         name='Знание и понимание видов тестирования')

    test_planning = SubSkill.objects.create(sub_position=test_manual,
                                            name='Планировать тестирование очередной версии (определение необходимых'
                                                 ' видов тестирования, тестового покрытия и т.д.)')
    test_report = SubSkill.objects.create(sub_position=test_manual,
                                          name='Составление отчетности по итогам и прогрессу тестирования')
    test_analysis = SubSkill.objects.create(sub_position=test_manual, name='Анализ результатов тестирования')

    test_instrumentariy = SubSkill.objects.create(sub_position=test_auto,
                                                  name='Владение инструментарием (консоль, VCS, CI, '
                                                       'IDE и т.п.)')
    programming = SubSkill.objects.create(sub_position=test_auto, name='Начальный опыт программирования')
    test_results = SubSkill.objects.create(sub_position=test_auto, name='Разбор результатов прохождения тестов')

    app_contents_understanding = SubSkill.objects.create(sub_position=test_auto,
                                                         name='Уверенное понимание устройства тестируемого приложения '
                                                              '(составные компоненты, используемые технологии)')
    auto_importance = SubSkill.objects.create(sub_position=test_auto,
                                              name='Понимание целесообразности применения автоматизации')
    test_lead_skills = SubSkill.objects.create(sub_position=test_lead,
                                               name='Навык руководства процессом тестирования')

    localization = Skill.objects.create(position=test, name='Умение локализовать проблему и грамотно её описать')
    bug_tracker = Skill.objects.create(position=test, name='Умение работать с багтрекером')
    life_cycle = Skill.objects.create(position=test, name='Понимание жизненного цикла продукта')
    test_process = Skill.objects.create(position=test, name='Понимание процесса тестирования')
    business_importance = Skill.objects.create(position=test,
                                               name='Понимание важности разрабатываемого функционала для бизнеса')

    learning = Skill.objects.create(position=test, name='Обучение младших сотрудников')
    test_theory = Skill.objects.create(position=test, name='Знание теории тестирования, понимание целесообразности '
                                                           'применения различных видов тестирования')
    test_improve = Skill.objects.create(position=test, name='Помощь в улучшении производственных процессов')
    test_methods = Skill.objects.create(position=test, name='Понимание методик разработки через тестирование')

    test_instruments_lvl_1 = SubSkillLevel.objects.create(sub_skill=test_instruments, name='Junior level', weight=1,
                                                          description='Перечислить инструменты совместной работы, их предназначение;\n'
                                                                      'рассказать подробно о testms;\n'
                                                                      'рассказать об используемых тестовых стендах;\n'
                                                                      'где проводится тестирование;\n'
                                                                      'как задеплоить новый билд для тестирования.')
    bug_documenting_lvl_1 = SubSkillLevel.objects.create(sub_skill=bug_documenting, name='Junior level', weight=1,
                                                         description='Что делать, если проблема повторяется нестабильно;\n'
                                                                     'что делать, если проблема не повторяется;\n\n'
                                                                     'ПРАКТИЧЕСКОЕ ЗАДАНИЕ: показать свои багрепорты;\n'
                                                                     'ПРАКТИЧЕСКОЕ ЗАДАНИЕ: показать примеры неправильных багрепортов'
                                                                     ' (своих или чужих), проанализировать, '
                                                                     'что неправильно и как можно улучшить')
    get_log_lvl_1 = SubSkillLevel.objects.create(sub_skill=get_log, name='Junior level', weight=1,
                                                 description='Где тестируемое приложение ведет логи;\n'
                                                             'как найти в логе информацию, относящуюся к '
                                                             'исследуемой проблеме')
    test_documentation_lvl_2 = SubSkillLevel.objects.create(sub_skill=test_documentation, name='Middle level', weight=2,
                                                            description='Что такое тест-дизайн;\n'
                                                                        'чем отличается тесткейс от чеклиста;\n'
                                                                        'какие методики применяются для определения набора '
                                                                        'тестов, которые необходимо провести.\n\n'
                                                                        'Функциональное тестирование:\n'
                                                                        '\tРассказать о методике ACC '
                                                                        '(attributes-capabilities-components);\n'
                                                                        '\tперечислить attributes, capabilities, components'
                                                                        ' тестируемого продукта.\n \n'
                                                                        'Какие ещё характеристики (attributes) можно'
                                                                        ' тестировать (характеристики качества ПО)?\n'
                                                                        'Какие вы знаете виды нефункционального тестирования '
                                                                        '(как минимум 5 штук). Как его проводить? \n'
                                                                        'Какая тестовая документация,'
                                                                        'рассказать алгоритм создания набора тестов для'
                                                                        ' тестирования фичи, как декомпозировать требования '
                                                                        'в тесткейсы. \n\nПРАКТИЧЕСКОЕ ЗАДАНИЕ: составить набор '
                                                                        'тестов для тестирования какой-нибудь бытовой вещи '
                                                                        '(калькулятор, офисный стул, лифт и т.д.)')
    test_types_lvl_2 = SubSkillLevel.objects.create(sub_skill=test_types, name='Middle level', weight=2,
                                                    description='Kакие виды тестирования существуют, как их можно '
                                                                'классифицировать: функциональное, нефункциональное.'
                                                                'Kакие виды тестирования вы применяете в работе')
    test_planning_lvl_3 = SubSkillLevel.objects.create(sub_skill=test_planning, name='Senior level', weight=3,
                                                       description='Oписать используемую методику оценки трудозатрат'
                                                                   ' на тестирование (очередного релиза);\n'
                                                                   'как определить какие виды тестирования '
                                                                   'необходимо проводить; \n\nнагрузочное тестирование:'
                                                                   'как провести нагрузочное тестирование, '
                                                                   'необходимые этапы и артефакты')
    test_report_lvl_3 = SubSkillLevel.objects.create(sub_skill=test_report, name='Senior level', weight=3,
                                                     description='Kак оценить качество продукта?\n'
                                                                 ' Как понять, насколько он качественный: '
                                                                 'тестовое покрытие, процент автоматизации')
    test_analysis_lvl_3 = SubSkillLevel.objects.create(sub_skill=test_analysis, name='Senior level', weight=3,
                                                       description='Kакой критерий релиза (как определяется, '
                                                                   'что можно релизить); \nчто такое тестовое покрытие; '
                                                                   'предложения по улучшению ручного тестирования')

    test_instrumentariy_lvl_1 = SubSkillLevel.objects.create(sub_skill=test_instrumentariy, name='Junior level',
                                                             weight=1,
                                                             description='Kак развернуть копию продукта на рабочей машине, '
                                                                         'какие инструменты использовать, какие команды вводить;\n'
                                                                         'Рассказать по шагам, как создать новый функциональный'
                                                                         ' автотест, используя имеющийся инструментарий - '
                                                                         'от создания метода, до включения его в регулярный'
                                                                         ' запуск автотестов;\n'
                                                                         'pассказать по шагам, как создать новый нагрузочный '
                                                                         'автотест, используя имеющийся инструментарий;\n'
                                                                         'что такое профиль нагрузки;\n'
                                                                         'pассказать об API, который предоставляет фреймворк '
                                                                         'автоматического тестирования для создания автотестов')
    test_instrumentariy_lvl_2 = SubSkillLevel.objects.create(sub_skill=test_instrumentariy, name='Middle level',
                                                             weight=2,
                                                             description='Pассказать про паттерн PageObject, как он '
                                                                         'реализован во фреймворке;\n'
                                                                         'рассказать как написать класс для работы с '
                                                                         'новой страницей (на примере);\n'
                                                                         'как в дальнейшем тесты смогут использовать'
                                                                         ' этот созданный класс (напрямую или через '
                                                                         'сервисный слой);\n рассказать про фреймворк '
                                                                         'функционального тестирования (с использованием'
                                                                         ' Selenium Webdriver), какие сторонние '
                                                                         'библиотеки используются и для чего; \nкак во '
                                                                         'фреймворке реализованы ожидания: загрузки '
                                                                         'страницы, смены статусов элементов '
                                                                         '(появление, исчезновение, enable, '
                                                                         'disable, etc.), AJAX-взаимодействий; \n'
                                                                         'как во фреймворке реализована работа с '
                                                                         'таблицами (гридами); \nдругие интересные фичи '
                                                                         'фреймворка, реализован ли в каком-либо виде '
                                                                         'DSL? Для чего он нужен? Варианты реализации.\n'
                                                                         'Как работают библиотеки для автоматизации '
                                                                         'тестирования семейства cucumber')
    test_instrumentariy_lvl_3 = SubSkillLevel.objects.create(sub_skill=test_instrumentariy, name='Senior level',
                                                             weight=3,
                                                             description='Pассказать про SOLID; \n'
                                                                         'рассказать о практическом опыте создания инструментов.\n'
                                                                         'Pассказать устройство фреймворка автоматизации'
                                                                         ' функционального тестирования: \n\tобщая архитектура,'
                                                                         'архитектурные слои,'
                                                                         'объяснить как работает автоматизация от уровня'
                                                                         ' тестов до Selenium и браузера; \nесть ли '
                                                                         'возможность параллельного запуска тестов,'
                                                                         'есть ли возможность из теста работать с '
                                                                         'несколькими инсталляциями приложения или '
                                                                         'несколькими приложениями, \nесть ли возможность'
                                                                         ' из теста работать не только с уровнем UI, '
                                                                         'но и другими (заглянуть в логи, БД и т.д.);\n'
                                                                         'каким образом решается проблема нестабильных '
                                                                         '(хрупких) тестов; \nсколько времени выполняются '
                                                                         'UI-тесты, как можно ускорить их прохождение,\n'
                                                                         'насколько высокий порог использования фреймворка,'
                                                                         ' как его можно снизить; \nкак работает Selenium,'
                                                                         'как работать с Selenium GRID')
    auto_importance_lvl_3 = SubSkillLevel.objects.create(sub_skill=auto_importance,
                                                         name='Senior level', weight=3,
                                                         description='Pассказать о плюсах и минусах '
                                                                     'использования автоматизации тестирования;\n'
                                                                     'в каких случаях автоматизация не '
                                                                     'целесообразна; \nв случае применения '
                                                                     'автоматизации что необходимо '
                                                                     'автоматизировать в первую очередь; \nкак '
                                                                     'определить процент использования '
                                                                     'автоматизации в тестировании продукта;\n'
                                                                     'предложения по улучшению автоматического тестирования')
    app_contents_understanding_lvl_2 = SubSkillLevel.objects.create(sub_skill=app_contents_understanding,
                                                                    name='Middle level', weight=2,
                                                                    description='Какие сторонние библиотеки используются и для чего;'
                                                                                'Как реализованы различные функциональные части приложения')
    programming_lvl_1 = SubSkillLevel.objects.create(sub_skill=programming, name='Junior level', weight=1,
                                                     description='три кита ООП;'
                                                                 'Kак происходит загрузка страницы в браузере: '
                                                                 'какие протоколы, аппаратные, '
                                                                 'программные компоненты задействованы?')
    test_results_lvl_1 = SubSkillLevel.objects.create(sub_skill=test_results, name='Junior level', weight=1,
                                                      description='Функциональных:\n\tкак запустить автотесты на проекте,'
                                                                  'кому приходят нотификации о результатах тестирования,'
                                                                  'где смотреть результаты,\n\t'
                                                                  'какие могут быть результаты, что делать в каждом случае,\n\t'
                                                                  'все тесты прошли,есть упавшие тесты,билд не собрался,\n\t'
                                                                  'как определить, где проблема (в тесте или продукте) '
                                                                  'при падении теста; \nнагрузочных:\n\t'
                                                                  'как анализировать результаты (рассказать на примере из практики)')

    test_lead_skills_lvl_4 = SubSkillLevel.objects.create(sub_skill=test_lead_skills, name='Lead level', weight=4,
                                                          description='Умение организовать тестирование продукта с нуля '
                                                                      '(ручное и автоматизированное); \nумение спроектировать '
                                                                      'фреймворк для автоматизации тестирования; \nумение '
                                                                      'организовать инфраструктуру для автоматического '
                                                                      'тестирования (как функционального, так и нагрузочного '
                                                                      'и др.); \nразвитие сотрудников; \nобеспечение бесперебойной'
                                                                      ' работы подчиненного подразделения')

    localization_lvl_1 = SkillLevel.objects.create(skill=localization, name='Junior level', weight=1,
                                                   description='Локализация проблемы\n\t'
                                                               'как определить, что наблюдаемое поведение системы является багом?\n\t'
                                                               'что значит локализовать проблему?\n\t'
                                                               'какие способы локализации (на примере)?\n\t'
                                                               'какая последовательность действий должна быть у тестировщика,'
                                                               ' если он обнаружил баг?\n\t'
                                                               'где искать информацию о проблеме;\n'
                                                               'багрепорт: \n\tчто это такое, для чего он нужен,\n\t'
                                                               'кто для чего его использует, \n\tшаблон багрепорта, '
                                                               'какое поле для чего нужно, \n\tжизненный цикл багрепорта,'
                                                               'какая информация должна в нём содержаться,'
                                                   )
    bugtrack_lvl_1 = SkillLevel.objects.create(skill=bug_tracker, name='Junior level', weight=1,
                                               description='Рассказать workflow бага и задачи;\n'
                                                           'Какие статусы у бага, задачи;\n'
                                                           'В какой момент и кто меняет статус;\n'
                                                           'Последовательность изменения статусов (workflow) бага,задачи;'
                                                           'Привязка багов и задач к релизам')
    life_cycle_lvl_2 = SkillLevel.objects.create(skill=life_cycle, name='Middle level', weight=2,
                                                 description='Из каких фаз состоит жизненный цикл продукта, кто '
                                                             'ответственен за ту или иную фазу? \nВ какой момент ЖЦ продукта'
                                                             ' следует подключаться тестировщикам, какие функции выполняет'
                                                             ' тестировщик в рабочем процессе, за что отвечает, '
                                                             'с кем взаимодействует, какую пользу продукту приносит?\n'
                                                             'Каким образом обеспечивается качество продукта всеми '
                                                             'участниками процесса')
    test_process_lvl_2 = SkillLevel.objects.create(skill=test_process, name='Middle level', weight=2,
                                                   description='Kак проходит тестирование очередного релиза,'
                                                               ' какие тесты проводятся; \nсколько проводится итераций '
                                                               'тестирования релиза, какова цель многократного тестирования')
    business_importance_lvl_2 = SkillLevel.objects.create(skill=business_importance, name='Middle level', weight=2,
                                                          description='Какие задачи пользователи решают с помощью продукта;\n'
                                                                      'Какой функционал является основным;\n'
                                                                      'В чём ценность разрабатываемого продукта для рынка;'
                                                                      '(optional) Отличия от конкурирующих продуктов')
    learning_lvl_3 = SkillLevel.objects.create(skill=learning, name='Senior level', weight=3,
                                               description='Как организовано обучение младших сотрудников, '
                                                           'Ваша роль в этом процессе')
    test_theory_lvl_3 = SkillLevel.objects.create(skill=test_theory, name='Senior level', weight=3)
    test_improve_lvl_3 = SkillLevel.objects.create(skill=test_improve, name='Senior level', weight=3,
                                                   description='Рассказать о своей работе;\n'
                                                               'о проекте;о своей роли в процессе разработки;\n'
                                                               'как устроен процесс разработки проекта; \n'
                                                               'как организована документация по функционалу проекта; \n'
                                                               'как организована тестовая документация;\n'
                                                               'какими задачами занимаетесь, какие успехи, какие сложности,'
                                                               ' какие планы; \nкаким образом оценивается качество продукта '
                                                               'сейчас; \nкак (по 10 балльной шкале) можно оценить качество'
                                                               ' продукта; \nпредложения по повышению качества продукта')
    test_methods_lvl_3 = SkillLevel.objects.create(skill=test_methods, name='Senior level', weight=3,
                                                   description='для чего используется TDD, BDD, ATDD, etc')

    andrey_emp3_local = EmployeeSkillLevel.objects.create(employee=andrey_emp3, skill_level=localization_lvl_1)
    andrey_emp3_tracker = EmployeeSkillLevel.objects.create(employee=andrey_emp3, skill_level=bugtrack_lvl_1)
    andrey_emp3_instrs = EmployeeSubSkillLevel.objects.create(employee=andrey_emp3,
                                                              sub_skill_level=test_instruments_lvl_1)
    andrey_emp3_bug = EmployeeSubSkillLevel.objects.create(employee=andrey_emp3, sub_skill_level=bug_documenting_lvl_1)
    andrey_emp3_log = EmployeeSubSkillLevel.objects.create(employee=andrey_emp3, sub_skill_level=get_log_lvl_1)
    andrey_emp3_instrii = EmployeeSubSkillLevel.objects.create(employee=andrey_emp3,
                                                               sub_skill_level=test_instrumentariy_lvl_1)

    dilyara_empl_local = EmployeeSkillLevel.objects.create(employee=dilyara_empl, skill_level=localization_lvl_1)
    dilyara_empl_tracker = EmployeeSkillLevel.objects.create(employee=dilyara_empl, skill_level=bugtrack_lvl_1)
    dilyara_empl_instrii = EmployeeSubSkillLevel.objects.create(employee=dilyara_empl,
                                                                sub_skill_level=test_instrumentariy_lvl_1)
    dilyara_empl_program = EmployeeSubSkillLevel.objects.create(employee=dilyara_empl,
                                                                sub_skill_level=programming_lvl_1)
    dilyara_empl_testres = EmployeeSubSkillLevel.objects.create(employee=dilyara_empl,
                                                                sub_skill_level=test_results_lvl_1)
    dilyara_empl_log = EmployeeSubSkillLevel.objects.create(employee=dilyara_empl,
                                                            sub_skill_level=get_log_lvl_1)
    nikita_emp2_methods = EmployeeSkillLevel.objects.create(employee=nikita_emp2, skill_level=test_methods_lvl_3)
    nikita_emp2_improve = EmployeeSkillLevel.objects.create(employee=nikita_emp2, skill_level=test_improve_lvl_3)
    nikita_emp2_theory = EmployeeSkillLevel.objects.create(employee=nikita_emp2, skill_level=test_theory_lvl_3)
    nikita_emp2_learning = EmployeeSkillLevel.objects.create(employee=nikita_emp2, skill_level=learning_lvl_3)
    nikita_emp2_importance = EmployeeSubSkillLevel.objects.create(employee=nikita_emp2,
                                                              sub_skill_level=auto_importance_lvl_3)
    nikita_emp2_instr = EmployeeSubSkillLevel.objects.create(employee=nikita_emp2, sub_skill_level=test_instrumentariy_lvl_3)
    nikita_emp2_analysis = EmployeeSubSkillLevel.objects.create(employee=nikita_emp2, sub_skill_level=test_analysis_lvl_3)
    nikita_emp2_report = EmployeeSubSkillLevel.objects.create(employee=nikita_emp2,
                                                               sub_skill_level=test_report_lvl_3)
    nikita_emp2_planning = EmployeeSubSkillLevel.objects.create(employee=nikita_emp2,
                                                              sub_skill_level=test_planning_lvl_3)

    print('OK')
