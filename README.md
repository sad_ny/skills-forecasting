## Начало работы с репозиторием

Выбираем у себя на устройстве  директорию, в которой будет храниться локальная версия репозитория. Заходим в эту директорию, открываем Git Bash и в командной строке вводим команду, которую получаем после нажатия кнопочки Clone на странице репозитория, её шаблон:

**git clone https://***Your Bitbucket's nick***@bitbucket.org/sad_ny/skills-forecasting.git**

Должна появиться папка skills-forecasting. Это ваш *локальный репозиторий*. В той же командной строке Git Bash'а:

**cd skills-forecasting/ **

Теперь Вы внутри проекта, в командной строке Bash'а появится заветное (*master*).

---

## Работа с ветками

У нас будет 2 legacy-ветки - это **master** и **dev**.

**master** - там будет лежать релизная версия проекта, то есть артифакт проекта, полученный путем одного недельного спринта.

**dev** - здесь будет лежать текущая ***рабочая*** версия проекта, то есть в любой момент времени артифакт этой ветки должен быть рабочим.

Ваша работа с ветками как разработчика заключается в:

0. **git checkout dev** - переключаемся на ветку *dev*, где у нас лежит рабочая версия проекта. Возможно у вас возникнет ошибка, что  такой локальной ветки(*dev*) не существует. Тогда просто добавьте **-b** перед названием ветки (**git checkout -b dev**).
1. **git pull origin dev**
2. **git checkout -b branch_name** - создание собственной ветки.  *Имя ветки должно отражать то, что в ней разрабатывается: новый модуль, новая фича, фикс багов и т.д.*
3. Примеры:  **auth**, **profile_viev_fix**, **forecasting_feature**.
4. **Коммитьте** каждое изменение/добавление чего-то (*не надо уходить в крайность и коммитить каждую строчку кода,но и не стоит коммитить только в конце сеанса разработки. Написали модели сотрудника, закоммитили с соответствующим сообщением и продолжайте кодить дальше*).
5. После завершения сеанса разработки запушьте все Ваши **коммиты** в вашу(*созданную во 2-ом пункте*) ветку.
6. **git push origin branch_name** (*2-ой пункт*)
7. Если вы завершили свой таск - зайдите в раздел **Branches** на Bitbucket'е, там, в строчке с информацией о вашей ветке, будет *слово-кнопка* **create**. Она будет в колонке **Pull request**.
8. Нажимаете на неё. Там будет 2 поля с выбором ветки. Левая - это **source** - там автоматически выбрана Ваша ветка. Правая - **destination**. Там выбираете ветку **dev**.
9. Если вы уверены, что при следующем сеансе разработки вы перейдете в другую ветку, то поставьте еще галочку в пункте **close branch**. Это ни на что не повлияет, в будущем Вы сможете снова создать ветку с идентичным именем, но у Нас в репозитории не начнут скапливаться ветки.
10. И потом Я попытаюсь смерджить вашу ветку с **dev'ом**. Если все пройдет без проблем - то Ваш код уйдет в ветку **dev**, если же нет - то придется фиксить :С

---

## Начало работы над проектом

В папке проекта/в локальном репозитории открываете **cmd**. Выполняете ряд команд:

0. Если у вас уже была какая-то БД, то лучше снесите её (*удалите*).
1. **python manage.py migrate** - накатываете миграции (создаете структуру БД, создается сама БД).
2. **python manage.py fill_db** - заполняете БД тестовыми данными.

---

## Рекомендации по написанию сообщений коммитов

Копипаст с интернета, но советы реально полезные:

1. Make the titles always imperative, it should continue the phrase **"this commit was made to…”**.
2. Use ***present time*** in your messages.
3. **Remove** unnecessary punctuation marks.
4. **Use** small case (not consistent with Git commands but looks better).
5. Place everything in one row.
6. **Make a single commit for a single change** (***even really small***).
7. Point out the path to the file, if it's **not obvious** which one has been changed by the commit.
8. **Never use** dates, names and other inappropriate marks.
9. Consider your team to define the unified style for commit messages and always follow it. (***Think about it***)
10. Specify the type of commit, as it provides useful context about rest of its title:

	'feature' — for the new feature adding to a particular level of an application;
	
	‘fix' — for a serious bug fixing;
	
	'style' — for styling changes and formatting fixes;
	
	'refactor' — for refactoring the code of an application;
	
	'test' — for everything related to testing;
	
	'docs' — for everything related to documentation;
	
	'chore' — for regular code maintenance.

---

## Можно почитать

[Книжка по Гиту](https://habr.com/ru/post/150673/) (***Хотя бы 2-ую и 3-ью главу***)